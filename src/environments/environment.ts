export const environment = {
  production: false,
  debugApi: false,
  protocol: 'http',
  domain: 'localhost:9000',
  // domain: '201.240.29.92:9002',
  // domain: '636687cb.ngrok.io',
  apiName: 'api'
};
