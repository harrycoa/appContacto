export const environment = {
  production: true,
  debugApi: false,
  protocol: 'https',
  domain: 'de0e8c95.ngrok.io',
  apiName: 'api'
};
