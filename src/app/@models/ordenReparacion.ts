export class OrdenReparacion {
  placa: string;
  numero: number;
  nombre: string;
  km: number;
  servicio: string;
  telefono: string;
  fechaInicio: string;
  fechaFin: string;
  serie: string;
  motor: string;
  chasis: string;
  vin: string;
  anioFabricacion: string;
  constructor(
    placa?: string,
    numero?: number,
    nombre?: string,
    km?: number,
    servicio?: string,
    telefono?: string,
    fechaInicio?: string,
    fechaFin?: string,
    serie?: string,
    motor?: string,
    chasis?: string,
    vin?: string,
    anioFabricacion?: string
  ) {
    this.placa = placa ? placa : null;
    this.numero = numero ? numero : null;
    this.nombre = nombre ? nombre : null;
    this.km = km ? km : null;
    this.servicio = servicio ? servicio : null;
    this.telefono = telefono ? telefono : null;
    this.fechaInicio = fechaInicio ? fechaInicio : null;
    this.fechaFin = fechaFin ? fechaFin : null;
    this.serie = serie ? serie : null;
    this.motor = motor ? motor : null;
    this.chasis = chasis ? chasis : null;
    this.vin = vin ? vin : null;
    this.anioFabricacion = anioFabricacion ? anioFabricacion : null;
  }
}
