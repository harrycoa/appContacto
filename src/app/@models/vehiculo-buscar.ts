export class VehiculoBuscar {
  serie: string;
  placa: string;
  motor: string;
  chasis: string;
  vin: string;
  anioFabricacion: string;
  modelo: string;
  cliente: string;
  telefono: string;
  telefono2: string;
  telefono3: string;
  constructor() {
    this.serie = '';
    this.placa = '';
    this.motor = '';
    this.chasis = '';
    this.vin = '';
    this.anioFabricacion = '';
    this.modelo = '';
    this.cliente = '';
    this.telefono = '';
    this.telefono2 = '';
    this.telefono3 = '';
  }
}
