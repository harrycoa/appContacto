export class ContactoDetalle {
  detalle: string;
  detalleDescripcion: string;
  estado: string;
  constructor() {
    this.detalle = null;
    this.detalleDescripcion = null;
    this.estado = null;
  }
}
