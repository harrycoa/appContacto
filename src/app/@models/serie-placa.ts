export class SeriePlaca {
  serie: string;
  placa: string;
  motor: string;
  chasis: string;
  vin: string;
  anioFabricacion: number;
  modelo: string;
  cliente: string;
  telefono: string;
  telefono2: string;

  constructor() {
    this.serie = null;
    this.placa = null;
    this.motor = null;
    this.chasis = null;
    this.vin = null;
    this.anioFabricacion = null;
    this.modelo = null;
    this.cliente = null;
    this.telefono = null;
    this.telefono2 = null;
  }
}
