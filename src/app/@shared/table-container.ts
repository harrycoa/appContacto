import {
  Component,
  Input,
  AfterViewInit,
  Renderer2,
  Inject,
  ChangeDetectionStrategy
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { WINDOW } from '@core/window';

@Component({
  selector: 'table-container',
  template: `
    <div class="relative" id="{{ idContainer }}">
      <div class="loading-sp" *ngIf="loading">
        <mat-spinner
          *ngIf="loading"
          strokeWidth="2"
          diameter="50"
        ></mat-spinner>
      </div>
      <div class="overflow-auto" id="{{ idContainer }}1">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      .relative {
        position: relative;
      }
      .loading-sp {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.15);
        z-index: 1000;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .overflow-auto {
        overflow: auto;
      }
    `
  ]
})
export class TableContainer implements AfterViewInit {
  idContainer: string;
  @Input() heightSub: number;
  @Input() loading: boolean;

  constructor(
    private r2: Renderer2,
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window: Window
  ) {
    this.idContainer = new Date().getTime().toString();
  }

  ngAfterViewInit() {
    this.heightTable();
  }

  heightTable() {
    if (this.heightSub) {
      const el = this.document.getElementById(this.idContainer);
      const el1 = this.document.getElementById(`${this.idContainer}1`);
      const h =
        this.window.innerHeight ||
        this.document.documentElement.clientHeight ||
        this.document.body.clientHeight;

      let height = h - this.heightSub;
      height = height > 400 ? height : 400;
      this.r2.setStyle(el, 'height', `${height}px`);
      this.r2.setStyle(el1, 'height', `${height}px`);
    }
  }
}
