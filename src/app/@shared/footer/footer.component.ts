import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-footer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  @Input() texto: string;

  constructor() {
    this.texto = this.texto
      ? this.texto
      : `DevBug © ${new Date().getFullYear()}`;
  }
}
