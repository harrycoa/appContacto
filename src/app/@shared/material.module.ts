import { NgModule } from '@angular/core';

import { FlexLayoutModule } from '@angular/flex-layout';

import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatSidenavModule,
  MatListModule,
  MatIconModule,
  MatToolbarModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatTooltipModule,
  MatTableModule,
  MatRadioModule,
  MatPaginatorModule,
  MatTabsModule,
  MatSelectModule,
  MatDialogModule,
  MatCheckboxModule,
  MatSlideToggleModule,
  MatSliderModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MatChipsModule
} from '@angular/material';

// CDK

// SCROLL MODULE
import { ScrollbarModule } from 'ngx-scrollbar';
import { CdkTableModule } from '@angular/cdk/table';

const MODULES = [
  FlexLayoutModule,
  ScrollbarModule,

  MatCardModule,
  MatButtonModule,
  MatSidenavModule,
  MatListModule,
  MatIconModule,
  MatToolbarModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatButtonModule,
  MatTooltipModule,
  MatTableModule,
  MatRadioModule,
  MatPaginatorModule,
  MatTabsModule,
  MatSelectModule,
  MatDialogModule,
  MatCheckboxModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatInputModule,
  MatFormFieldModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MatChipsModule,
  CdkTableModule

];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES]
})
export class MaterialModule {}
