import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ui-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div fxLayout="row" fxLayoutAlign="center" class="mat-typography">
            <ng-content></ng-content>
    </div>
    `
})
export class UiContainerComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
