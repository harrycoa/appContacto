import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { timeTo24h } from '@shared/functions';

@Component({
  selector: 'datetime-picker',
  templateUrl: './datetime-picker.component.html'
})
export class DateTimePickerComponent implements OnInit {
  fecha;
  hora = '12:00 PM';

  @Input() label: string;

  @Output()
  change = new EventEmitter<any>();

  constructor() {}
  dayFilter = (d: Date) => d.getDay() !== 0;

  ngOnInit() {
    this.fecha = null;
  }

  changeFecha(fecha) {
    this.fecha = fecha;

    this.setFechaHora();
    this.change.emit(this.fecha);
  }

  changeHora(hora) {
    this.hora = hora;

    if (this.fecha != null) this.setFechaHora();
    this.change.emit(this.fecha);
  }

  setFechaHora() {
    const h = timeTo24h(this.hora);
    (this.fecha as Date).setHours(h.horas, Number(h.minutos), 0);
  }
}
