import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from '@shared/material.module';

import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from '@shared/footer/footer.component';
import { SideNavComponent } from '@shared/side-nav/side-nav.component';
import { UiContainerComponent } from '@shared/ui-container/ui-container.component';
import { TimePickerModule } from '@shared/time-picker/time-picker.module';
import { SearchTwoPickerComponent } from './search-two-picker/search-two-picker.component';
import { DateTimePickerComponent } from './datetime-picker/datetime-picker.component';
import { TableContainer } from './table-container';

const MODULES = [MaterialModule, TimePickerModule];
const COMPONENTS = [
  NavBarComponent,
  SideNavComponent,
  FooterComponent,
  UiContainerComponent,
  SearchTwoPickerComponent,
  DateTimePickerComponent,
  TableContainer
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [...MODULES, CommonModule, RouterModule, FormsModule],
  exports: [...MODULES, ...COMPONENTS]
})
export class SharedModule {}
