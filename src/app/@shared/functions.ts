import { environment as env } from 'environments/environment';

export function setUrl(uri: string) {
  const domain = `${env.protocol}://${env.domain}`;
  const subdomain = env.debugApi ? '/' : `/${env.apiName}/`;
  const URL = domain + subdomain + uri;
  return URL;
}

export function aammddHHmmss(fechayhora: string): any {
  const fechahora = fechayhora.split(' ');
  const fecha = fechahora[0];
  const hora = fechahora[1];
  const ff = fecha.split('/');

  const fh = `${ff[2]}-${ff[1]}-${ff[0]} ${hora}`;
  return new Date(fh) as any;
}
/**
 *
 * @param fecha fecha en formato dd/mm/aaaa convierte a  dd-mm-aaaa
 */
export function aaaammdd(fecha: string): any {
  const ff = fecha.split('/');
  return `${ff[2]}-${ff[1]}-${ff[0]}`;
}

export function timeTo24h(timeStr: string) {
  const colon = timeStr.indexOf(':');
  const hours = timeStr.substr(0, colon),
    minutes = timeStr.substr(colon + 1, 2),
    meridian = timeStr.substr(colon + 4, 2).toUpperCase();
  let hoursInt = parseInt(hours, 10);
  const offset = meridian == 'PM' ? 12 : 0;

  if (hoursInt === 12) hoursInt = offset;
  else hoursInt += offset;

  return { horas: hoursInt, minutos: minutes };
}

/**
 *
 * @param fecha fecha en formato dd/mm/aaaa convierte a aaa-mm-dd
 */
export function toDateMySql(fecha: string): any {
  const ff = fecha.split('/');
  return `${ff[2]}-${ff[1]}-${ff[0]}`;
}
//ya chato
