

export const ListRecojos = [
    {idRecojo:1,recojo:'RECOJO DE TARJETA'},
    {idRecojo:2,recojo:'RECOJO DE PLACA'},
    {idRecojo:3,recojo:'RECOJO DE TARJETA Y PLACA'}
  ];

export const ListInvitaciones = [
    {idInvitacion:1,invitacion:'WHATSAPP NO CONTESTADO'},
    {idInvitacion:2,invitacion:'LA UNIDAD OPERA FUERA DE CUSCO'},
    {idInvitacion:3,invitacion:'NO DESEA REALIZAR SSC'},
    {idInvitacion:4,invitacion:'REALIZA EL SERVICIO EN OTRO CONSECIONARIO'},
    {idInvitacion:5,invitacion:'REQUIERE MAYOR INFORMACION SOBRE EL SSC'},
    {idInvitacion:6,invitacion:'OTRO'}
  ];



export const ListRegistroClientesAD = [
    {idRCAD:1,registroCliente: 'ACLARACION INMEDIATA DOCUMENTADA'},
    {idRCAD:2,registroCliente: 'ACLARACION INMEDIATA NO DOCUMENTADA'},
    {idRCAD:3,registroCliente: 'REQUIERE RETORNO A TALLER, QUEJA EN TALLER'},
    {idRCAD:4,registroCliente: 'REQUIERE RETORNO A TALLER, PRUEBA DE RUTA'},
    {idRCAD:5,registroCliente: 'OTROS'}
  ];
export const ListRegistroClientesAI = [
    {idRCAI:1,registroCliente: 'DESCUENTO'},
    {idRCAI:2,registroCliente: 'SERVICIO GRATUITO'},
    {idRCAI:3,registroCliente: 'OTROS'}
  ];

export const ListAgenda1K =[
    {idA_1:1,agenda: 'RECORDATORIO MANTTO 1k (SEGUNDO RECORDATORIO o más)'},
    {idA_1:2,agenda: 'RECORDATORIO MANTTO 1k (PRIMER RECORDATORIO)'},
    {idA_1:3,agenda: 'RECORDATORIO MANTTO 5K HIACE (PRIMER RECORDATORIO)'},
    {idA_1:4,agenda: 'RECUPERACIÓN DE CLIENTES QUE NO ASISTIERON A 1K LUEGO DE TRANSCURRIDOS 30 DÍAS CALENDARIO LUEGO DE LA ENTREGA DE SU PLACA'}
];