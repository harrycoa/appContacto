import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy
} from '@angular/core';

@Component({
  selector: 'side-nav',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit, OnDestroy {
  items = LIST;

  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {}
}
export interface Arbol {
  name: string;
  route: string;
  children: Arbol[];
}
const LIST: Arbol[] = [
  {
    name: 'Procedimientos Supervisión',
    route: '',
    children: [
      {
        name: 'Atención Denuncias por Deficiencias (094-2017-OS/CD)',
        route: '',
        children: [
          {
            name: 'Deficiencias Servicio Electricidad (RHD)',
            route: 'rhd',
            children: []
          }
        ]
      },
      {
        name: 'Facturación (115-2017-OS/CD)',
        route: '',
        children: [
          {
            name: 'Anexo 3 - Registro Facturación',
            route: 'anexo/3',
            children: []
          },
          {
            name: 'Anexo 4 - BD Suministros Facturados',
            route: 'anexo/4',
            children: []
          }
        ]
      }
    ]
  },
  {
    name: 'Búsqueda Suministro',
    route: 'buscar-suministro',
    children: []
  }
];
