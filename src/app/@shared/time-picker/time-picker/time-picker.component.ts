import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { TimeDialogComponent } from '../time-dialog/time-dialog.component';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'time-picker',
  templateUrl: 'time-picker.component.html',
  styleUrls: ['time-picker.component.scss']
})
export class TimePickerComponent {

  @Input() time: string;
  @Input() placeholder: string;
  @Input() disabled: boolean;
  @Input() disabledInput: boolean;
  @Input() required: string;
  @Output() change: EventEmitter<string> = new EventEmitter<string>();

  constructor(private dialog: MatDialog) { }

  showPicker($event) {
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      data: {
        hour: this.getHour(this.time),
        minute: this.getMinute(this.time),
        meriden: this.getMeridien(this.time)
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) return;
      else if (result !== -1) {
        this.time = `${result.hour}:${this.formatMinute(result.minute)} ${result.meriden}`;
        this.change.emit(this.time);
      }
    });
    return false;
  }

  formatMinute(m): string {
    if (m < 10) return '0' + String(m);
    else return String(m);
  }

  getHour(time: string) {
    return Number(time.split(':')[0]);
  }
  getMinute(time: string) {
    const hour = (time.split(' '))[0];
    return Number(hour.split(':')[1]);
  }
  getMeridien(time: string) {
    return (time.split(' '))[1];
  }
}
