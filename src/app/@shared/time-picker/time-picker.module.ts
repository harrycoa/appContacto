import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimePickerComponent } from './time-picker/time-picker.component';
import { ClockComponent } from './clock/clock.component';
import { TimeDialogComponent } from './time-dialog/time-dialog.component';
import {
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatDialogModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    FlexLayoutModule,

    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule
  ],
  declarations: [
    TimePickerComponent,
    ClockComponent,
    TimeDialogComponent
  ],
  exports: [
    TimePickerComponent
  ],
  entryComponents: [
    TimeDialogComponent
  ],
})
export class TimePickerModule { }
