import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CLOCK_TYPE } from '../clock/clock.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'time-dialog',
  templateUrl: 'time-dialog.component.html',
  styleUrls: ['time-dialog.component.scss']
})
export class TimeDialogComponent {

  userTime: any = {};
  VIEW_HOURS = CLOCK_TYPE.HOURS;
  VIEW_MINUTES = CLOCK_TYPE.MINUTES;
  currentView: CLOCK_TYPE = this.VIEW_HOURS;

  constructor(
    @Inject(MAT_DIALOG_DATA) userTimeData,
    private dialogRef: MatDialogRef<TimeDialogComponent>,
    cdRef: ChangeDetectorRef) {

    this.userTime = userTimeData;
  }

  formatMinute(): string {

    if (this.userTime.minute < 10) {

      return '0' + String(this.userTime.minute);
    } else {

      return String(this.userTime.minute);
    }
  }

  setCurrentView(type: CLOCK_TYPE) {

    this.currentView = type;
  }

  setMeridien(m: string) {

    this.userTime.meriden = m;
  }

  revert() {

    this.dialogRef.close(-1);
  }

  submit() {

    this.dialogRef.close(this.userTime);
  }
}
