import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  EventEmitter,
  Output
} from '@angular/core';
import { toDateMySql } from '@shared/functions';

@Component({
  selector: 'search-two-picker',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './search-two-picker.component.html',
  styleUrls: ['./search-two-picker.component.scss']
})
export class SearchTwoPickerComponent implements OnInit, OnDestroy {
  fechaInicio = null;
  fechaFin = null;
  @Output() buscar = new EventEmitter<any>();

  constructor() {}
  dayFilter = (d: Date): boolean => d.getDay() !== 0;
  ngOnInit() {}
  ngOnDestroy() {}

  buscarDatos() {
    let fechaInicio = this.fechaInicio as Date;
    let fechaFin = this.fechaFin as Date;
    fechaInicio = toDateMySql(fechaInicio.toLocaleDateString());
    fechaFin = toDateMySql(fechaFin.toLocaleDateString());
    const values = { fechaInicio, fechaFin };
    this.buscar.emit(values);
  }
}
