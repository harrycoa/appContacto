import {
  Component,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  Input
} from '@angular/core';
import { Usuario } from '@core/auth/usuario';

@Component({
  selector: 'nav-bar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {
  _usuario: Usuario;
  @Input()
  set usuario(usuario: Usuario) {
    this._usuario = usuario;
  }
  @Input() iconoAccion: string;
  @Input() textoAccion: string;

  @Output() clickMenu: EventEmitter<any> = new EventEmitter();
  @Output() logout: EventEmitter<any> = new EventEmitter();
  constructor() {}
}
