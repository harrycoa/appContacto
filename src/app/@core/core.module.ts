// angular
import { NgModule, Optional, SkipSelf, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { MAT_LABEL_GLOBAL_OPTIONS } from '@angular/material';

import { JWTModule } from '@core/jwt.module';
import { AuthModule } from './auth/auth.module';
import { LoadingModule } from './loading/loading.module';

import { ResultHttpInterceptor } from '@core/resultHttp.interceptor';
// import { HttpService } from '@core/services/http.service';

import { environment as env } from 'environments/environment';

import { registerLocaleData } from '@angular/common';
import localeEsPE from '@angular/common/locales/es-PE';
import localeEsPeExtra from '@angular/common/locales/extra/es-PE';
import { WINDOW, _window } from './window';

registerLocaleData(localeEsPE, localeEsPeExtra);

const MODULES = [
  // angular
  CommonModule,
  HttpClientModule,
  ServiceWorkerModule.register('/ngsw-worker.js', {
    enabled: env.production
  }),

  // app
  AuthModule.forRoot(),
  LoadingModule.forRoot(),
  JWTModule
];

const PROVIDERS = [
  { provide: LOCALE_ID, useValue: 'es-PE' },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ResultHttpInterceptor,
    multi: true
  },
  { provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: { float: 'always' } },
  { provide: WINDOW, useFactory: _window }
  // HttpService
];

@NgModule({
  imports: [...MODULES],
  providers: [...PROVIDERS]
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('CoreModule ya está cargado. Importar solo en AppModule');
    }
  }
}
