import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { swalError, notifyInfo, swalInfo, swalOk } from '@shared/util';

@Injectable()
export class ResultHttpInterceptor implements HttpInterceptor {
  constructor(private router: Router) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) this.eventAction(event);
        },
        error => {
          if (error instanceof HttpErrorResponse) this.errorAction(error);
        }
      )
    );
  }

  eventAction(res: HttpResponse<any>) {
    switch (res.status) {
      case 201:
        // notifyOk(res.body['message']);
        swalOk(res.body['message'], '');
        break;
      case 204:
        notifyInfo(res.statusText);
        break;
      default:
        break;
    }
  }
  errorAction(error: HttpErrorResponse) {
    const title = error.statusText;
    const message =
      error.error.Message == undefined ? error.message : error.error.Message;

    switch (error.status) {
      case 401:
        swalInfo(title, message);
        localStorage.clear();
        this.router.navigate(['/login']);
        break;
      case 412:
        swalError(error.error.message, '');
        break;
      case 500:
        swalError(title, message);
        break;
      default:
        break;
    }
  }
}
