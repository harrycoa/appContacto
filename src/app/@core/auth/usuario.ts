export interface Authenticate {
  username: string;
  password: string;
}

export interface AuthResponse {
  mensaje: string;
  key: string;
}

export class Usuario {
  nombreUsuario: string;
  nombreCompleto: string;
  estado: number;
  dni: string;
  email: string;
  telefono: string;
  rol: number[];

  constructor(obj: UserObj) {
    this.nombreUsuario = (obj && obj.nombreUsuario) || null;
    this.nombreCompleto = (obj && obj.nombreCompleto) || null;
    this.estado = (obj && obj.estado) || null;
    this.dni = (obj && obj.dni) || null;
    this.email = (obj && obj.email) || null;
    this.telefono = (obj && obj.telefono) || null;
    this.rol = (obj && obj.rol) || [];
  }
}

interface UserObj {
  nombreUsuario?: string;
  nombreCompleto?: string;
  estado?: number;
  dni?: string;
  email?: string;
  telefono?: string;
  rol?: number[];
}
