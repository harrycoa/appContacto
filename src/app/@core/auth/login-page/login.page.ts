import { Component, OnInit } from '@angular/core';
import { AuthService } from '@core/auth/auth.service';

@Component({
  selector: 'login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  pending: boolean;
  error: string;

  constructor(private auth: AuthService) {}

  ngOnInit() {
    this.pending = false;
    this.error = null;
  }

  onSubmit(credenciales) {
    if (!this.pending) {
      this.pending = true;
      this.error = null;
      this.auth.auth(credenciales).subscribe(res => {
        this.pending = false;
        const login = this.auth.login(res);
        if (login.isLogIn) this.auth.goHome();
        else this.error = login.error;
      }, err => (this.pending = false));
    }
  }
}
