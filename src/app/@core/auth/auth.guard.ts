import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot
} from '@angular/router';

import { AuthService } from '@core/auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService) {}

  canActivate(): boolean {
    return this.checkLogin();
  }

  canActivateChild(): boolean {
    return this.canActivate();
  }

  checkLogin(): boolean {
    const isAuth = this.auth.isLogIn();

    if (isAuth) return true;

    this.auth.logout();
    return false;
  }
}

@Injectable()
export class RoleGuard implements CanActivate, CanActivateChild {
  constructor(private auth: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    return this.checkRol(route);
  }

  canActivateChild(route: ActivatedRouteSnapshot): boolean {
    return this.canActivate(route);
  }

  checkRol(route: ActivatedRouteSnapshot): boolean {
    if (route.data['rol'] == undefined) return true;

    const roles: number[] = route.data['rol'];
    const permisos = this.auth.getUser().rol;

    let access = false;
    for (let i = 0; i < roles.length; i++) {
      const rol = roles[i];
      access = permisos.includes(rol);
      if (access) break;
    }
    if (!access) {
      // notifyInfo('Acceso no autorizado!');
    }

    return access;
  }
}
