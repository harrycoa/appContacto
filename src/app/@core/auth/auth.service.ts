import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { JwtHelperService } from '@auth0/angular-jwt';

import { Authenticate, AuthResponse, Usuario } from './usuario';
import { setUrl } from '@shared/functions';
import { KEY } from '@shared/util';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  auth(user: Authenticate): Observable<AuthResponse> {
    const URL = setUrl('login');
    return this.http
      .post<AuthResponse>(URL, user)
      .pipe(catchError(e => throwError(e)));
  }

  login(data: AuthResponse): IsAuthenticated {
    let isExpired = true;
    if (data.key != '') {
      localStorage.setItem(KEY, data.key);
      const helper = new JwtHelperService();
      isExpired = helper.isTokenExpired(data.key);
      if (isExpired) localStorage.clear();
    }
    return { isLogIn: !isExpired, error: data.mensaje };
  }

  goHome() {
    this.router.navigate(['/']);
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  isLogIn() {
    const token = localStorage.getItem(KEY);
    const helper = new JwtHelperService();
    const isExpired = helper.isTokenExpired(token);
    return !isExpired;
  }

  getUser() {
    const token = localStorage.getItem(KEY);
    const helper = new JwtHelperService();
    const usuario: Usuario = helper.decodeToken(token).user;
    // const usuario: Usuario = new Usuario({});
    return usuario;
  }
}

export interface IsAuthenticated {
  isLogIn: boolean;
  error: string;
}
