import { Component, OnInit } from '@angular/core';

import { TiposService } from '../tipos.service';

@Component({
  selector: 'reporte',
  templateUrl: './reporte.page.html',
  styleUrls: ['./reporte.page.scss']
})
export class ReportePage implements OnInit {
  constructor(private service: TiposService) {}

  options = {
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    legend: {
      orient: 'vertical',
      x: 'left',
      data: ['Contestadas', 'No Contestadas']
    },
    series: [
      {
        name: 'LLamadas',
        type: 'pie',
        radius: ['60%', '80%'],
        avoidLabelOverlap: false,
        label: {
          normal: {
            show: false,
            position: 'center'
          },
          emphasis: {
            show: true,
            textStyle: {
              fontSize: '20',
              fontWeight: 'bold'
            }
          }
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        data: [
          { value: 335, name: 'Contestadas' },
          { value: 310, name: 'No Contestadas' }
        ]
      }
    ]
  };

  option = {
    xAxis: {
      type: 'category',
      data: [
        'Lunes',
        'Martes',
        'Miércoles',
        'Jueves',
        'Viernes',
        'Sábado',
        'Domingo'
      ]
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data: [120, 200, 150, 80, 70, 110, 130],
        type: 'bar'
      }
    ]
  };

  ngOnInit() {}
}
