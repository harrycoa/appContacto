import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Subject } from 'rxjs';
import { InputEventHC } from '../historial-contacto/historial-contacto.component';

@Component({
  selector: 'historial-contacto-page',
  templateUrl: './historial-contacto.page.html',
  styleUrls: ['./historial-contacto.page.scss']
})
export class HistorialContactoPage implements AfterViewInit {
  eSubject: Subject<InputEventHC> = new Subject<InputEventHC>();

  constructor() {}

  dayFilter = (d: Date): boolean => d.getDay() !== 0;

  ngAfterViewInit(): void {
    setTimeout(() => this.eSubject.next({ name: 'listar' }));
  }

  changeEvent(e: InputEventHC) {
    this.eSubject.next(e);
  }
}
