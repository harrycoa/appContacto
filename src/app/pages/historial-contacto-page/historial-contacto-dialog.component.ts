import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subject } from 'rxjs';
import { InputEventHC } from '../historial-contacto/historial-contacto.component';

@Component({
  selector: 'historial-contacto-dialog',
  template: `
    <mat-card fxLayout="column">
      <h3 align="center" class="cl-primary">
        <strong>Historial de Contactos</strong>
      </h3>
      <hr class="wd-100">
      <historial-contacto
        [buscar$]="eSubject.asObservable()"
      ></historial-contacto>
    </mat-card>
  `,
  styleUrls: ['./historial-contacto.page.scss']
})
export class HistorialContactoDialog implements AfterViewInit {
  eSubject: Subject<InputEventHC> = new Subject<InputEventHC>();

  constructor(
    public dialogRef: MatDialogRef<HistorialContactoDialog>,
    @Inject(MAT_DIALOG_DATA) public placa: string
  ) {
    dialogRef.backdropClick().subscribe(() => dialogRef.close());
    dialogRef.keydownEvents().subscribe(e => {
      if (e.keyCode == 27) dialogRef.close();
    });
  }

  ngAfterViewInit() {
    setTimeout(() =>
      this.eSubject.next({ name: 'buscarPlaca', value: this.placa })
    );
  }
}
