import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';

import { tap } from 'rxjs/operators';

import { DialogOrdenComponent } from '../dialog-orden/dialog-orden.component';
import { AuthService } from '@core/auth/auth.service';
import { TiposService } from '../tipos.service';

import { Usuario } from '@core/auth/usuario';

@Component({
  selector: 'contestar-llamada-page',
  templateUrl: './contestar-llamada.page.html',
  styleUrls: ['./contestar-llamada.page.scss']
})
export class ContestarLlamadaPage implements OnInit {
  usuario: Usuario;
  motivos;
  estados;
  respuestas;
  pendingTipos: boolean;
  pendingBuscar: boolean;

  placas: any[];
  cliente: { nombre: string; telefono: string };

  @ViewChild('f')
  f: any;
  saving: boolean;
  constructor(
    private auth: AuthService,
    private service: TiposService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.placas = [];
    this.usuario = this.auth.getUser();
    this.getTipos();
    this.cliente = { nombre: '', telefono: '' };
  }
  getTipos() {
    this.pendingTipos = true;
    this.getMotivos().subscribe(
      _res1 =>
        this.getEstados().subscribe(
          _res2 =>
            this.getRespuestas().subscribe(
              _res3 => (this.pendingTipos = false),
              _err => (this.pendingTipos = false)
            ),
          _err => (this.pendingTipos = false)
        ),
      _err => (this.pendingTipos = false)
    );
  }
  getMotivos() {
    return this.service
      .listarMotivos('A')
      .pipe(tap(res => (this.motivos = res)));
  }
  getEstados() {
    return this.service.listarEstados().pipe(tap(res => (this.estados = res)));
  }
  getRespuestas() {
    return this.service
      .listarRespuesta()
      .pipe(tap(res => (this.respuestas = res)));
  }

  enviar(llamada) {
    this.saving = true;
    this.service
      .guardarLlamada(llamada)
      .subscribe(
        _res => (this.saving = false),
        _err => (this.saving = false),
        () => this.f.reset()
      );
  }

  buscarOrden(placa) {
    this.pendingBuscar = true;
    this.service.buscarOrden(placa).subscribe(
      res => {
        this.pendingBuscar = false;
        this.placas = res as any;
        this.showDialog(this.placas);
      },
      _err => (this.pendingBuscar = false)
    );
  }
  verOrden() {
    this.showDialog(this.placas);
  }
  showDialog(data) {
    const options = {
      data,
      height: '90%',
      width: '94%',
      panelClass: 'dialog-error'
    };
    this.dialog
      .open(DialogOrdenComponent, options)
      .afterClosed()
      .subscribe(cliente => (this.cliente = cliente));
  }
  verResultados() {
    if (this.placas.length > 0) {
      this.showDialog(this.placas);
    }
  }
}
