import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { toDateMySql } from '@shared/functions';
import { InputEventHC } from '../historial-contacto/historial-contacto.component';

@Component({
  selector: 'historial-contacto-filtro',
  templateUrl: './historial-contacto-filtro.component.html',
  styleUrls: ['./historial-contacto-filtro.component.scss']
})
export class HistorialContactoFiltroComponent implements OnInit {
  fechaInicio = null;
  fechaFin = null;

  @Output() event = new EventEmitter<InputEventHC>();

  constructor() {}
  dayFilter = (d: Date): boolean => d.getDay() !== 0;

  ngOnInit() {}

  buscar(value: string) {
    this.event.emit({ name: 'buscarPlaca', value });
  }
  buscarFecha() {
    if (this.fechaInicio != null && this.fechaFin != null) {
      let fechaInicio: any = this.fechaInicio as Date;
      let fechaFin: any = this.fechaFin as Date;
      fechaInicio = toDateMySql(fechaInicio.toLocaleDateString());
      fechaFin = toDateMySql(fechaFin.toLocaleDateString());
      this.event.emit({ name: 'buscarFecha', fechaInicio, fechaFin });
    }
  }
  filtrar(value: string) {
    this.event.emit({ name: 'filtrar', value });
  }
  download() {
    this.event.emit({ name: 'descargar' });
  }
}
