import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { setUrl } from '@shared/functions';

@Injectable()
export class EntregaUnidadesService {
  constructor(private http: HttpClient) {}

  buscarEntrega(serie) {
    const URL = setUrl(`busquedaVehicular/${serie}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  verificarEntrega(value) {
    const URL = setUrl(`bentrega/${value}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  guardarDatos(objeto) {
    const URL = setUrl(`entregaVehicular`);
    return this.http.post(URL, objeto).pipe(catchError(e => throwError(e)));
  }
}
