import { Component, OnInit, ViewChild } from '@angular/core';
import { EntregaUnidadesService } from './entrega-unidades.service';
import { swalWarn, swalOk } from '@shared/util';
import { Resultado } from '@models/resultado';

@Component({
  selector: 'entrega-unidades',
  templateUrl: './entrega-unidades.page.html',
  styleUrls: ['./entrega-unidades.page.scss']
})
export class EntregaUnidadesComponent implements OnInit {
  @ViewChild('f')
  f: any;
  saving: boolean;

  entrega: verificarEntrega;
  constructor(private service: EntregaUnidadesService) {}

  ngOnInit() {
    this.entrega = new verificarEntrega();
  }

  save(datosForm) {
    datosForm.telefono = datosForm.telefono.toString();
    datosForm.celular = datosForm.celular.toString();
    this.saving = true;
    this.service.guardarDatos(datosForm).subscribe(
      _res => (this.saving = false),
      _err => (this.saving = false),
      () => {
        this.f.reset();
        this.entrega = new verificarEntrega();
      }
    );
  }
  verificar(value) {
    this.saving = true;
    this.entrega = new verificarEntrega();
    this.service.verificarEntrega(value).subscribe(
      (res: verificarEntrega) => {
        this.saving = false;
        if (res.resultado.id == 1) swalWarn(res.resultado.mensaje, '');
        // else swalOk(res.resultado.mensaje, '');
        this.entrega = res;
      },
      _err => (this.saving = false)
    );
  }
}

class verificarEntrega {
  resultado: Resultado;
  entregaVehicular: datosEntrega;
  constructor() {
    this.resultado = new Resultado();
    this.entregaVehicular = null;
  }
}

interface datosEntrega {
  modelo: string;
  motor: string;
  chasis: string;
  vin: string;
  anioFabricacion: string;
  personaEntrega: string;
  telefono: string;
  celular: string;
  opinionEntrega: string;
  fechaEntregaVehiculo: string;
  fechaCitaReferencial2: string;
  // constructor() {
  //   this.idVehiculoEntrega = null;
  //   this.modelo = '';
  //   this.motor = '';
  //   this.chasis = '';
  //   this.vin = '';
  //   this.anioFabricacion = '';
  //   this.personaEntrega = '';
  //   this.telefono = '';
  //   this.celular = '';
  //   this.opinionEntrega = '';
  //   this.fechaEntregaVehiculo = '';
  //   this.fechaCitaReferencial2 = '';
  // }
}
