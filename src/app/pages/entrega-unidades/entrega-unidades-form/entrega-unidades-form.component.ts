import {
  Component,
  OnInit,
  ViewChild,
  EventEmitter,
  Output
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { TiposService } from 'app/pages/tipos.service';
import { EntregaUnidadesService } from '../entrega-unidades.service';
import { timeTo24h } from '@shared/functions';
import { VehiculoBuscar } from '@models/vehiculo-buscar';

@Component({
  selector: 'entrega-unidades-form',
  templateUrl: './entrega-unidades-form.component.html',
  styleUrls: ['./entrega-unidades-form.component.scss']
})
export class EntregaUnidadesFormComponent implements OnInit {
  form: FormGroup;
  pendienteBuscar: boolean;

  usosVehiculo = UsosVehiculo;
  departamentos: any[];
  provincias: any[];

  horaProgramacionEntrega;
  horaEntregaVehiculo;

  @Output()
  enviar = new EventEmitter<any>();

  @Output()
  verificar = new EventEmitter<any>();

  @ViewChild('f')
  f: any;

  constructor(
    private fb: FormBuilder,
    private service: TiposService,
    private sVehiculo: EntregaUnidadesService
  ) {}

  dayFilter = (d: Date): boolean => d.getDay() !== 0;

  ngOnInit() {
    this.initialize();
  }
  initialize() {
    this.departamentos = [];
    this.provincias = [];
    this.horaProgramacionEntrega = '12:00 PM';
    this.horaEntregaVehiculo = '12:00 PM';

    this.initForm();

    this.listarDepartamentos();
  }

  //#region formulario
  reset() {
    this.horaProgramacionEntrega = '12:00 PM';
    this.horaEntregaVehiculo = '12:00 PM';

    this.f.resetForm();
    this.resetForm();
  }

  initForm() {
    this.form = this.fb.group({
      serieTDP: [null],
      serie: [null],
      placa: [null],
      motor: [null],
      chasis: [null],
      vin: [null],
      anioFabricacion: [null],
      modelo: [null, Validators.required],
      fechaProgramacionEntrega: [{ value: null, disabled: true }],
      fechaEntregaVehiculo: [null, Validators.required],
      personaEntrega: [null, Validators.required],
      telefono: ['', Validators.required],
      celular: [''],
      opinionEntrega: [null],
      fechaCitaReferencial2: [null],
      usoVehicular: [null],
      idDepartamento: [null],
      idProvincia: [null]
    });
  }
  resetForm() {
    this.form.reset({
      serieTDP: null,
      serie: null,
      placa: null,
      motor: null,
      chasis: null,
      vin: null,
      anioFabricacion: null,
      modelo: null,
      fechaProgramacionEntrega: null,
      fechaEntregaVehiculo: null,
      personaEntrega: null,
      telefono: '',
      celular: '',
      opinionEntrega: null,
      fechaCitaReferencial2: null,
      usoVehicular: null,
      idDepartamento: null,
      idProvincia: null
    });
  }

  //#endregion

  buscar(selModelo, e: Event) {
    e.stopPropagation();
    selModelo.focus();
    this.buscarVehiculo(this.form.get('serieTDP').value);
  }

  buscarVehiculo(serieTdp) {
    this.pendienteBuscar = true;
    this.sVehiculo.buscarEntrega(serieTdp).subscribe(
      (vehiculo: VehiculoBuscar) => {
        this.pendienteBuscar = false;
        this.form.patchValue({
          serie: vehiculo.serie,
          placa: vehiculo.placa,
          motor: vehiculo.motor,
          chasis: vehiculo.chasis,
          vin: vehiculo.vin,
          anioFabricacion: vehiculo.anioFabricacion,
          modelo: vehiculo.modelo
        });
      },
      _err => (this.pendienteBuscar = false)
    );
  }

  listarDepartamentos() {
    this.service.listarDepartamentos().subscribe((departamentos: any[]) => {
      this.departamentos = departamentos;
    });
  }

  listarProvinciaDepartamento(idDepartamento) {
    this.service
      .listarProvinciaDepartamento(idDepartamento)
      .subscribe((provincias: any[]) => {
        this.provincias = provincias;
      });
  }

  changeFechaProgramacionEntrega(fecha) {
    this.setFechaHoraProgramacionEntrega();
  }
  changeHoraProgramacionEntrega(hora) {
    this.horaProgramacionEntrega = hora;
    if (this.form.get('fechaProgramacionEntrega').value != null)
      this.setFechaHoraProgramacionEntrega();
  }
  setFechaHoraProgramacionEntrega() {
    const h = timeTo24h(this.horaProgramacionEntrega);
    const fechaProgramacionEntrega: Date = this.form.get(
      'fechaProgramacionEntrega'
    ).value as Date;
    fechaProgramacionEntrega.setHours(h.horas, Number(h.minutos), 0);
    this.form.patchValue({ fechaProgramacionEntrega });
  }

  changeFechaEntrega(fecha) {
    this.setFechaHoraEntrega();
  }
  changeHoraEntrega(hora) {
    this.horaEntregaVehiculo = hora;
    if (this.form.get('fechaEntregaVehiculo').value != null)
      this.setFechaHoraEntrega();
  }
  setFechaHoraEntrega() {
    const h = timeTo24h(this.horaEntregaVehiculo);
    const fechaEntregaVehiculo: Date = this.form.get('fechaEntregaVehiculo')
      .value as Date;
    fechaEntregaVehiculo.setHours(h.horas, Number(h.minutos), 0);
    this.form.patchValue({ fechaEntregaVehiculo });
  }

  changeDepartamento(idDepartamento) {
    if (idDepartamento != null)
      this.listarProvinciaDepartamento(idDepartamento);
  }

  onSubmit() {
    // console.log(this.form.value);
    // console.log(this.form.getRawValue());
    this.enviar.emit(this.form.getRawValue());
  }

  verificarVehiculo(value) {
    this.verificar.emit(value);
  }
}

const UsosVehiculo = [
  'Empresa Urbano',
  'Empresa Interurbano',
  'Gobierno',
  'Particular Domestico',
  'Particular Trabajo',
  'Transporte Urbano',
  'Trasnporte Interurbano',
  'Taxista'
];
