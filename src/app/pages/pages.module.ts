import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { PagesRouting } from './pages.routing';

import { LlamarClienteFormComponent } from './llamar-cliente-form/llamar-cliente-form.component';
import { DatosBasicosComponent } from './llamar-cliente-form/datos-basicos/datos-basicos.component';
import { MotivoEstadoComponent } from './llamar-cliente-form/motivo-estado/motivo-estado.component';

import { MotivoB1Component } from './llamar-cliente-form/motivo-b1/motivo-b1.component';
import { MotivoB2Component } from './llamar-cliente-form/motivo-b2/motivo-b2.component';
import { MotivoB3Component } from './llamar-cliente-form/motivo-b3/motivo-b3.component';
import { MotivoB5Component } from './llamar-cliente-form/motivo-b5/motivo-b5.component';
import { MotivoB8Component } from './llamar-cliente-form/motivo-b8/motivo-b8.component';
import { MotivoB9Component } from './llamar-cliente-form/motivo-b9/motivo-b9.component';
import { MotivoB10Component } from './llamar-cliente-form/motivo-b10/motivo-b10.component';
import { MotivoB11Component } from './llamar-cliente-form/motivo-b11/motivo-b11.component';
import { MotivoB12Component } from './llamar-cliente-form/motivo-b12/motivo-b12.component';
import { MotivoB13Component } from './llamar-cliente-form/motivo-b13/motivo-b13.component';
import { MotivoB14Component } from './llamar-cliente-form/motivo-b14/motivo-b14.component';
import { MotivoB15Component } from './llamar-cliente-form/motivo-b15/motivo-b15.component';
import { MotivoB16Component } from './llamar-cliente-form/motivo-b16/motivo-b16.component';
import { MotivoB17Component } from './llamar-cliente-form/motivo-b17/motivo-b17.component';

import { AList4 } from './agenda/agenda-1k/a-list-4/a-list-4';
import { AList3 } from './agenda/agenda-1k/a-list-3/a-list-3';
import { AList1 } from './agenda/agenda-1k/a-list-1/a-list-1';
import { AgendaComponent } from './agenda/agenda.page';
import { EntregaUnidadesComponent } from './entrega-unidades/entrega-unidades.page';

import { ContainerPage } from './container.page';
import { LlamarClientePage } from './llamar-cliente-page/llamar-cliente.page';
import { ContestarLlamadaPage } from './contestar-llamada-page/contestar-llamada.page';
import { TiposService } from './tipos.service';
import { DialogOrdenComponent } from './dialog-orden/dialog-orden.component';
import { ReportePage } from './reporte/reporte.page';
import { HistorialContactoPage } from './historial-contacto-page/historial-contacto.page';
import { HistorialContactoDialog } from './historial-contacto-page/historial-contacto-dialog.component';
import { HistorialContactoComponent } from './historial-contacto/historial-contacto.component';
import { InicioComponent } from './inicio/inicio.component';
import { FormularioComponent } from './formulario/formulario.component';
import { ExcelService } from './excel.service';
import { NgxEchartsModule } from 'ngx-echarts';
import { GestionVozNegativaPage } from './gestion-voz-negativa/gestion-voz-negativa.page';

import { AList2 } from './agenda/agenda-1k/a-list-2/a-list-2';
import { Agenda1kComponent } from './agenda/agenda-1k/agenda-1k.page';
import { Agenda5kComponent } from './agenda/agenda-5k/agenda-5k.page';
import { BList1 } from './agenda/agenda-5k/b-list-1/b-list-1';
import { BList2 } from './agenda/agenda-5k/b-list-2/b-list-2';
import { BList3 } from './agenda/agenda-5k/b-list-3/b-list-3';

import { AgendaNotifPostHeComponent } from './agenda/agenda-notif-post-he/agenda-notif-post-he.page';
import { EList1 } from './agenda/agenda-notif-post-he/e-list-1/e-list-1';

import { AgendarCitaComponent } from './llamar-cliente-form/agendar-cita/agendar-cita.component';
import { FechaCitaComponent } from './llamar-cliente-form/fecha-cita/fecha-cita.component';
import { ServiciosCitaComponent } from './llamar-cliente-form/servicios-cita/servicios-cita.component';
import { RazonNoCitaComponent } from './llamar-cliente-form/razon-no-cita/razon-no-cita.component';
// tslint:disable-next-line:max-line-length
import { DetalleRazonNoCitaComponent } from './llamar-cliente-form/detalle-razon-no-cita/detalle-razon-no-cita.component';

import { EntregaUnidadesFormComponent } from './entrega-unidades/entrega-unidades-form/entrega-unidades-form.component';
import { EntregaUnidadesService } from './entrega-unidades/entrega-unidades.service';
// tslint:disable-next-line:max-line-length
import { GestionVozNegativaFormComponent } from './gestion-voz-negativa/gestion-voz-negativa-form/gestion-voz-negativa-form.component';
// tslint:disable-next-line:max-line-length
import { GestionVozNegativaListComponent } from './gestion-voz-negativa/gestion-voz-negativa-list/gestion-voz-negativa-list.component';
// tslint:disable-next-line:max-line-length
import { GestionVozNegativaComponent } from './gestion-voz-negativa/gestion-voz-negativa/gestion-voz-negativa.component';

import { HistorialEntregaUnidadesPage } from './historial-entrega-unidades-page/historial-entrega-unidades.page';
import { AgendaService } from './agenda/agenda.service';
import { Pronostico5kPage } from './agenda/pronostico5k.page';
import { HistorialContactoFiltroComponent } from './historial-contacto-filtro/historial-contacto-filtro.component';

import { EncuestaComponent } from './llamar-cliente-form/encuesta/encuesta.component';
import { PreguntaComponent } from './llamar-cliente-form/encuesta/pregunta.component';

// AGENDAS
import { AgendaSsc } from './agenda/agenda-ssc/agenda-ssc';
import { PostServicio } from './agenda/post-servicio/post-servicio';
import { AgendaVozNegativa } from './agenda/agenda-voz-negativa/agenda-voz-negativa';
import { IncumplimientoCita } from './agenda/imcumplimiento-cita/incumplimiento-cita';
import { NotificacionTerminoTrabajo } from './agenda/notificacion-termino-trabajo/notificacion-termino-trabajo';

const MODULES = [
  CommonModule,
  PagesRouting,
  SharedModule,
  ReactiveFormsModule,
  FormsModule,
  NgxEchartsModule
];
const COMPONENTS = [
  ContainerPage,
  LlamarClientePage,
  ContestarLlamadaPage,
  DialogOrdenComponent,
  EncuestaComponent,
  PreguntaComponent,
  EntregaUnidadesComponent,
  GestionVozNegativaPage,
  AgendaComponent,
  Agenda1kComponent,
  AList1,
  AList2,
  AList3,
  AList4,
  Agenda5kComponent,
  BList1,
  BList2,
  BList3,
  AgendaNotifPostHeComponent,
  EList1,

  ReportePage,
  HistorialContactoPage,
  HistorialContactoComponent,
  InicioComponent,
  FormularioComponent,
  LlamarClienteFormComponent,
  EntregaUnidadesComponent,
  AgendaComponent,
  DatosBasicosComponent,
  MotivoEstadoComponent,

  MotivoB1Component,
  MotivoB2Component,
  MotivoB3Component,
  MotivoB5Component,
  MotivoB8Component,
  MotivoB9Component,
  MotivoB10Component,
  MotivoB11Component,
  MotivoB12Component,
  MotivoB13Component,
  MotivoB14Component,
  MotivoB15Component,
  MotivoB16Component,
  MotivoB17Component,

  AgendarCitaComponent,
  FechaCitaComponent,
  ServiciosCitaComponent,
  RazonNoCitaComponent,
  DetalleRazonNoCitaComponent,

  EntregaUnidadesFormComponent,
  Pronostico5kPage,
  GestionVozNegativaFormComponent,
  GestionVozNegativaListComponent,
  GestionVozNegativaComponent,
  HistorialEntregaUnidadesPage,
  HistorialContactoFiltroComponent,
  HistorialContactoDialog,

  //Agendas
  NotificacionTerminoTrabajo,
  PostServicio,
  AgendaSsc,
  AgendaVozNegativa,
  IncumplimientoCita
];
const PROVIDERS = [
  TiposService,
  ExcelService,
  EntregaUnidadesService,
  AgendaService
];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS],
  providers: [...PROVIDERS],
  entryComponents: [
    DialogOrdenComponent,
    EncuestaComponent,
    HistorialContactoDialog
  ]
})
export class PagesModule {}
