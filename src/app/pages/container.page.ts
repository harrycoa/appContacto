import { Component, OnInit } from '@angular/core';

import { AuthService } from '@core/auth/auth.service';
import { Usuario } from '@core/auth/usuario';

@Component({
  selector: 'container-page',
  templateUrl: './container.page.html',
  styleUrls: ['./container.page.scss']
})
export class ContainerPage implements OnInit {
  usuario: Usuario;
  constructor(private auth: AuthService) {}

  ngOnInit() {
    this.usuario = this.auth.getUser();
    // this.usuario = new Usuario({ nombreCompleto: 'ALEXIS SALGADO TACURI' });
  }
  logout() {
    this.auth.logout();
  }
}
