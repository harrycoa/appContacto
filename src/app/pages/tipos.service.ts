import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { setUrl } from '@shared/functions';

@Injectable()
export class TiposService {
  constructor(private http: HttpClient) {}

  listarMotivos(mclase) {
    const URL = setUrl(`motivo/${mclase}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarServicios(mclase) {
    const URL = setUrl(`servicios/${mclase}14`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarEstados() {
    const URL = setUrl(`estado`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  listarEstadosPorClase(clase) {
    const URL = setUrl(`estado/${clase}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  buscarOrden(placa) {
    const URL = setUrl(`orden/${placa}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  buscarSeriePlaca(value) {
    const URL = setUrl(`busquedaVehicular/${value}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  guardarLlamada(objeto) {
    // const URL = setUrl(`incontacto`);
    const URL = setUrl(`insertarFormulario`);
    return this.http.post(URL, objeto).pipe(catchError(e => throwError(e)));
  }
  buscarLlamadasPlaca(placa) {
    const URL = setUrl(`historial/${placa}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  buscarOrdenesReparacionFecha(fechaInicial, fechaFinal) {
    const URL = setUrl(`historial/${fechaInicial}/${fechaFinal}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  listarLlamadas() {
    const URL = setUrl(`historial`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarRespuesta() {
    const URL = setUrl(`respuesta`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarVehiculoModelo() {
    const URL = setUrl(`vehiculomodelo`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  buscarVehiculoModelo(modelo: string) {
    const URL = setUrl(`vehiculomodelo/${modelo}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarUnidades() {
    const URL = setUrl(`listaentrega`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  //==============================
  //====== LISTADO DE AGENDA======
  //==============================
  listar_AList1() {
    //const URL = setUrl(`contacto`);
    //return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  listar_AList2() {
    //const URL = setUrl(`contacto`);
    //return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarResultados(idMotivo) {
    const URL = setUrl(`resultado/${idMotivo}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarRazones(idMotivo) {
    const URL = setUrl(`razon/${idMotivo}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  listarDepartamentos() {
    const URL = setUrl(`departamento`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  listarProvinciaDepartamento(idDepartamento) {
    const URL = setUrl(`provincia/${idDepartamento}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarGestionVozNegativa() {
    const URL = setUrl(`gestion`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  guardarGestionVozNegativa(objeto) {
    const URL = setUrl(`gnegativa`);
    return this.http.post(URL, objeto).pipe(catchError(e => throwError(e)));
  }

  /**FECHA LIMTE LLAMADA */
  fechaLimite(placa) {
    const URL = setUrl(`fechalimite/${placa}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  // fecha proxima llamada (pronostico)
  proximaLlamada(placa, sabekmActual, km) {
    const URL = setUrl(`proximallamada/${placa}/${sabekmActual}/${km}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
}
