import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { TiposService } from '../tipos.service';

@Component({
  selector: 'historial-entrega-unidades-page',
  templateUrl: './historial-entrega-unidades.page.html',
  styleUrls: ['./historial-entrega-unidades.page.scss']
})
export class HistorialEntregaUnidadesPage implements OnInit {
  displayedColumns: string[] = [
    'idVehiculoEntrega',
    'serie',
    'motor',
    'chasis',
    'vin',
    'anioFabricacion',
    'personaEntrega',
    'telefono',
    'celular',
    'opinionEntrega',
    'fechaEntregaVehiculo',
    'fechaCitaReferencial2'
  ];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  loadingData: boolean;

  // private excelService: ExcelService
  constructor(private service: TiposService) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.loadingData = true;
    this.service.listarUnidades().subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
      },
      _err => (this.loadingData = false),
      () => (this.loadingData = false)
    );
  }
}
