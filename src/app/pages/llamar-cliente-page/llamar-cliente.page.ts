import { Component, OnInit, ViewChild } from '@angular/core';
import { TiposService } from '../tipos.service';

@Component({
  selector: 'llamar-cliente-page',
  templateUrl: './llamar-cliente.page.html',
  styleUrls: ['./llamar-cliente.page.scss']
})
export class LlamarClientePage implements OnInit {
  @ViewChild('f')
  f: any;
  saving: boolean;
  constructor(private service: TiposService) {}

  ngOnInit() {}

  save(datosForm) {
    this.saving = true;
    this.service
      .guardarLlamada(datosForm)
      .subscribe(
        _res => (this.saving = false),
        _err => (this.saving = false),
        () => this.f.reset()
      );
  }
}
