import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  SimpleChange,
  ViewChild
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Usuario } from '@core/auth/usuario';

@Component({
  selector: 'formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit, OnChanges {
  @Input()
  usuario: Usuario;
  @Input()
  motivos;
  @Input()
  estados: any[];
  @Input()
  respuestas: any[];
  @Input()
  pendingTipos: boolean;
  @Input()
  pendingBuscar: boolean;

  @Input()
  clienteNombre: string;
  @Input()
  clienteTelefono: string;

  @Output()
  enviar = new EventEmitter<any>();
  @Output()
  buscarOrden = new EventEmitter<any>();
  @Output()
  verOrden = new EventEmitter<any>();

  form: FormGroup;
  horaRetomar;
  _estados;
  _respuestas;

  @ViewChild('f')
  f: any;

  constructor(private fb: FormBuilder) {}
  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0; // && day !== 6;
  }
  ngOnInit() {
    this.horaRetomar = '12:00 PM';
    this.form = this.fb.group(this.initForm());
    this.changeMotivo();
    this.changeEstadoLlamada();
    this.changeTipoContactoT();
    this.changeTipoContactoC();
  }
  ngOnChanges(changes: SimpleChanges) {
    const n: SimpleChange = changes.clienteNombre;
    const t: SimpleChange = changes.clienteTelefono;
    if (n) {
      if (this.form != undefined)
        this.form.patchValue({
          cliente: n.currentValue,
          contactoc: t.currentValue
        });
    }
  }
  initForm() {
    return {
      placa: ['', Validators.required],
      trabajadorCorasur: [this.usuario.nombreUsuario],
      tipoContactot: ['Email', Validators.required],
      contactot: [this.usuario.email, Validators.required],
      cliente: ['', Validators.required],
      tipoContactoc: ['Teléfono', Validators.required],
      contactoc: ['', Validators.required],
      idMotivo: [null, Validators.required],
      k: [{ value: null, disabled: true }, Validators.required],
      activo: ['', Validators.required],
      idEstado: [null, Validators.required],
      idRespuesta: [null, Validators.required],
      resultado: ['', Validators.required],
      km: [0, Validators.required],
      fechaRetomar: [null]
      // fechaRetomar: [{ value: null, disabled: true }, Validators.required]
    };
  }
  resetForm() {
    return {
      placa: '',
      trabajadorCorasur: this.usuario.nombreUsuario,
      tipoContactot: 'Email',
      contactot: this.usuario.email,
      cliente: '',
      tipoContactoc: '',
      contactoc: '',
      idMotivo: null,
      k: 0,
      activo: '',
      idEstado: null,
      idRespuesta: null,
      resultado: '',
      km: 0,
      fechaRetomar: null
    };
  }
  changeMotivo() {
    this.form.get('idMotivo').valueChanges.subscribe(idMotivo => {
      if (idMotivo == 19) this.form.get('k').enable();
      else {
        this.form.get('k').disable();
        this.form.patchValue({ k: null });
      }
    });
  }
  changeEstadoLlamada() {
    this.form.get('activo').valueChanges.subscribe(value => {
      const filtro = value == 'S' ? 'A' : 'B';
      this._estados = this.estados.filter(e => e.eClase == filtro);
      this._respuestas = this.respuestas.filter(r => r.rclase == filtro);

      if (value == 'S') {
        // this.form.get('fechaRetomar').enable();
        this.form.get('idRespuesta').enable();
        this.form.get('resultado').enable();
        this.form.get('km').enable();
      } else {
        // this.form.get('fechaRetomar').disable();
        this.form.get('idRespuesta').disable();
        this.form.get('resultado').disable();
        this.form.get('km').disable();
      }
    });
  }
  changeTipoContactoT() {
    this.form.get('tipoContactot').valueChanges.subscribe(value => {
      if (value == 'Email') {
        this.form.patchValue({ contactot: this.usuario.email });
      } else this.form.patchValue({ contactot: this.usuario.telefono });
    });
  }
  changeTipoContactoC() {
    this.form.get('tipoContactoc').valueChanges.subscribe(value => {
      switch (value) {
        case 'Email':
          this.form.patchValue({ contactoc: '' });
          break;
        case 'Teléfono':
          this.form.patchValue({ contactoc: this.clienteTelefono });
          break;
        default:
          this.form.patchValue({ contactoc: '' });
          break;
      }
    });
  }

  onSubmit() {
    if (this.form.valid) {
      if (this.form.get('fechaRetomar').value != null)
        this.formatToDate(this.horaRetomar);
      this.enviar.emit(this.form.value);
    }
  }
  reset() {
    this.f.resetForm();
    this.horaRetomar = '12:00 PM';
    this.form.reset(this.resetForm());
  }
  buscar(inputCliente, e: Event) {
    e.stopPropagation();
    inputCliente.focus();
    this.buscarOrden.emit(this.form.get('placa').value);
  }

  verResultados() {
    this.verOrden.emit();
  }

  formatToDate(timeStr: string) {
    const colon = timeStr.indexOf(':');
    const hours = timeStr.substr(0, colon),
      minutes = timeStr.substr(colon + 1, 2),
      meridian = timeStr.substr(colon + 4, 2).toUpperCase();
    let hoursInt = parseInt(hours, 10);
    const offset = meridian == 'PM' ? 12 : 0;

    if (hoursInt === 12) hoursInt = offset;
    else hoursInt += offset;

    const fechaRetomar: Date = this.form.get('fechaRetomar').value as Date;
    fechaRetomar.setHours(hoursInt, Number(minutes), 0);
    this.form.patchValue({ fechaRetomar });
  }
}
