import { AgendaComponent } from './agenda/agenda.page';
import { EntregaUnidadesComponent } from './entrega-unidades/entrega-unidades.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerPage } from './container.page';
import { LlamarClientePage } from './llamar-cliente-page/llamar-cliente.page';
import { ContestarLlamadaPage } from './contestar-llamada-page/contestar-llamada.page';

import { InicioComponent } from './inicio/inicio.component';
import { HistorialContactoPage } from './historial-contacto-page/historial-contacto.page';
import { ReportePage } from './reporte/reporte.page';
import { GestionVozNegativaPage } from './gestion-voz-negativa/gestion-voz-negativa.page';

// tslint:disable-next-line:max-line-length
import { GestionVozNegativaListComponent } from './gestion-voz-negativa/gestion-voz-negativa-list/gestion-voz-negativa-list.component';
// tslint:disable-next-line:max-line-length
import { GestionVozNegativaComponent } from './gestion-voz-negativa/gestion-voz-negativa/gestion-voz-negativa.component';
import { HistorialEntregaUnidadesPage } from './historial-entrega-unidades-page/historial-entrega-unidades.page';
import { Pronostico5kPage } from './agenda/pronostico5k.page';

const routes: Routes = [
  {
    path: '',
    component: ContainerPage,
    children: [
      { path: '', component: InicioComponent },
      { path: 'llamar-cliente', component: LlamarClientePage },
      { path: 'contestar-llamada', component: ContestarLlamadaPage },
      { path: 'historial', component: HistorialContactoPage },
      { path: 'reporte', component: ReportePage },
      { path: 'entrega-unidad', component: EntregaUnidadesComponent },
      {
        path: 'historial-entrega-unidad',
        component: HistorialEntregaUnidadesPage
      },
      {
        path: 'gestion-voz-negativa',
        component: GestionVozNegativaPage,
        children: [
          { path: '', component: GestionVozNegativaListComponent },
          { path: 'gestion', component: GestionVozNegativaComponent }
        ]
      },
      { path: 'agenda', component: AgendaComponent },
      { path: 'pronostico5k', component: Pronostico5kPage }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRouting {}
