import { Component, OnInit, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';

import { TiposService } from '../tipos.service';
import { ExcelService } from '../excel.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'historial-contacto',
  templateUrl: './historial-contacto.component.html',
  styleUrls: ['./historial-contacto.component.scss']
})
export class HistorialContactoComponent implements OnInit, OnDestroy {
  columnas: string[] = [
    'placa',
    'fechaRegistro',
    'cliente',
    'contactoCliente',
    'estado',
    'motivo',
    'seLogroContacto',
    'vozCliente', // resultadoSeLogroContacto
    'trabajadorCorasur'
  ];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  loadingData: boolean;

  private _buscarSubscription: any;
  @Input() buscar$: Observable<InputEventHC>;

  constructor(
    private service: TiposService,
    private excelService: ExcelService
  ) {}

  dayFilter = (d: Date): boolean => d.getDay() !== 0;

  ngOnInit() {
    this.initialize();
  }
  ngOnDestroy() {
    this._buscarSubscription.unsubscribe();
  }

  initialize() {
    this._buscarSubscription = this.buscar$.subscribe(e => {
      switch (e.name) {
        case 'listar':
          this.listar();
          break;
        case 'buscarPlaca':
          this.buscar(e.value);
          break;
        case 'buscarFecha':
          this.buscarPorFecha(e.fechaInicio, e.fechaFin);
          break;
        case 'filtrar':
          this.filtrar(e.value);
          break;
        case 'descargar':
          this.descargar();
          break;
      }
    });
  }

  listar() {
    this.consultar('listar');
  }
  buscar(placa) {
    this.consultar('buscar', placa);
  }
  buscarPorFecha(fechaInicio, fechaFin) {
    this.consultar('buscarPorFecha', fechaInicio, fechaFin);
  }
  consultar(nameService, p1?, p2?) {
    let service;
    switch (nameService) {
      case 'listar':
        service = this.service.listarLlamadas();
        break;
      case 'buscar':
        service = this.service.buscarLlamadasPlaca(p1);
        break;
      case 'buscarPorFecha':
        service = this.service.buscarOrdenesReparacionFecha(p1, p2);
        break;
    }
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.loadingData = true;
    service.subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
        this.loadingData = false;
      },
      _err => (this.loadingData = false)
    );
  }

  descargar() {
    let datos = [];
    if (this.dataSource.filteredData.length > 0)
      datos = this.dataSource.filteredData;
    else datos = this.dataSource.data;
    this.excelService.exportAsExcelFile(datos, 'Registro-Llamadas');
  }

  filtrar(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

export interface InputEventHC {
  name: string;
  fechaInicio?: string;
  fechaFin?: string;
  value?: string;
}
