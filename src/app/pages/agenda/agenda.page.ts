import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'agenda',
  templateUrl: './agenda.page.html',
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        width: 100%;
      }
      mat-card {
        padding: 12px;
      }
    `
  ]
})
export class AgendaComponent implements OnInit {
  ngOnInit() {}
}
