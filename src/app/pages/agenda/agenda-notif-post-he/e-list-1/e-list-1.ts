import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { TiposService } from 'app/pages/tipos.service';
import { ExcelService } from 'app/pages/excel.service';

@Component({
  selector: 'e-list-1',
  templateUrl: './e-list-1.html',
  styleUrls: ['./e-list-1.scss']
})
export class EList1 implements OnInit {
  displayedColumns: string[] = [
    'Nro',
    'Placa',
    'Asesor',
    'Contacto',
    'Telefono1',
    'Telefono2',
    'ServicioPactadoCita',
    'Hora_Programada',
    'Hora_Apertura',
    'Hora_Inicio',
    'Hora_Fin',
    'Trabajo_Adicional',
    'Numero_monto',
    'FechaYHora_Llamada',
    'Resultados_Contesta_Nocontesta',
    'Resultados_Acepta',
    'Resultados_FechaYHora',
    'VozCliente'
  ];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  loadingData: boolean;
  constructor(
    private service: TiposService,
    private excelService: ExcelService
  ) {}

  ngOnInit() {
    this.actualizar();
  }
  buscar(placa) {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loadingData = true;
    this.service.buscarLlamadasPlaca(placa).subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      _err => (this.loadingData = false),
      () => (this.loadingData = false)
    );
  }
  descargar() {
    this.excelService.exportAsExcelFile(
      this.dataSource.data,
      'Registro-Llamadas'
    );
  }

  actualizar() {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loadingData = true;
    /*this.service.listar_AList1().subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      _err => (this.loadingData = false),
      () => (this.loadingData = false)
    );//*/
    //por mientras hasta poder listar
    this.loadingData = false;
  }
  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
