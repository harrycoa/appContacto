import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { TiposService } from 'app/pages/tipos.service';
import { ExcelService } from 'app/pages/excel.service';

@Component({
  selector: 'a-list-3',
  templateUrl: './a-list-3.html',
  styleUrls: ['./a-list-3.scss']
})
export class AList3 implements OnInit {
  displayedColumns: string[] = [
    'Nro',
    'Placa_Serie',
    'Modelo',
    'Contacto',
    'Telefono1',
    'Telefono2',
    'Prioridad',
    'Fecha_EntregaUnidad',
    'Fecha_EntregaPlaca',
    'Fecha_CitaTentaEncuesta',
    'Fecha_CitaRatifiAgradec',
    'Fecha_SdoRecordatorioMantto',
    'ResultPrev_FechaWappPrevio',
    'ResultPrev_VV',
    'ResultPrev_Resultado',
    'ResultLLamada_EfectuadaFecha',
    'ResultLLamada_EfectuadaHora',
    'ResultLLamada_Contesta_Nocontesta',
    'ResultLLamada_Resultado',
    'ResultLLamada_FechaPactoVolverLLamar',
    'ResultLLamada_KmtrajeLlamada',
    'ResultLLamada_FechaCita',
    'ResultLLamada_Servicio'
  ];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  loadingData: boolean;
  constructor(
    private service: TiposService,
    private excelService: ExcelService
  ) {}

  ngOnInit() {
    this.actualizar();
  }
  buscar(placa) {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loadingData = true;
    this.service.buscarLlamadasPlaca(placa).subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      _err => (this.loadingData = false),
      () => (this.loadingData = false)
    );
  }
  descargar() {
    this.excelService.exportAsExcelFile(
      this.dataSource.data,
      'Registro-Llamadas'
    );
  }

  actualizar() {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loadingData = true;
    /*this.service.listar_AList2().subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      _err => (this.loadingData = false),
      () => (this.loadingData = false)
    );//*/
    //por mientras hasta poder listar
    this.loadingData = false;
  }
  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
