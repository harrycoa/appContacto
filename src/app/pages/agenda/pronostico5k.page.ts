import { Component } from '@angular/core';

@Component({
  selector: 'pronostico5k',
  template: `
    <mat-card fxLayout="column" fxLayoutGap="8px">
      <h3 class="cl-primary">Pronóstico 5k</h3>
      <b-list-1></b-list-1>
    </mat-card>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        width: 100%;
      }
      mat-card {
        padding: 12px;
      }
    `
  ]
})
export class Pronostico5kPage {}
