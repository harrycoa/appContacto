import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { AgendaService } from '../agenda.service';
import { ExcelService } from 'app/pages/excel.service';
import { Router } from '@angular/router';

@Component({
  selector: 'agenda-voz-negativa',
  templateUrl: './agenda-voz-negativa.html'
})
export class AgendaVozNegativa implements OnInit {
  fecha = new Date();

  metadata = metadata;
  displayColumns: string[] = [];
  loading;
  data: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private api: AgendaService,
    private excel: ExcelService,
    private router: Router
  ) {}

  dayFilter = (d: Date): boolean => d.getDay() !== 0;

  ngOnInit() {
    this.displayColumns = this.metadata.columnDef.map(c => c.property);
    this.listar();
  }

  listar() {
    this.setData();
    this.loading = true;
    this.api.listarNotificacionTF(this.fecha).subscribe(
      res => {
        this.setData(res);
        this.loading = false;
      },
      _err => (this.loading = false)
    );
  }
  setData(data?: any) {
    this.data = new MatTableDataSource(data);
    this.data.paginator = this.paginator;
  }

  filtrar(value: string) {
    this.data.filter = value.trim().toLowerCase();
    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  descargar() {
    let datos = [];
    if (this.data.filteredData.length > 0) datos = this.data.filteredData;
    else datos = this.data.data;
    this.excel.exportAsExcelFile(datos, 'Notificación Termino Trabajo');
  }

  selectRow(e) {
    sessionStorage.setItem('agendas', e.placa);
    this.router.navigate(['/llamar-cliente']);
  }
}

const metadata: any = {
  columnDef: [
    { title: 'Asesor',                      property: 'asesor',                   width:'250px'},
    { title: 'Placa',                       property: 'placa' ,                   width:'70px'},
    { title: 'Persona de Contacto',         property: 'cliente',                  width:'290px'},
    { title: 'Teléfono 1',                  property: 'telefono',                 width:'90px', background:'#673AB7'},
    { title: 'Teléfono 2',                  property: 'celular',                  width:'90px', background:'#673AB7'},
    { title: 'Servicio Pactado en Cita',    property: 'servicio',                 width:'250px',background:'#2196f3'},
    { title: 'Ingreso Taller',              property: 'ingresoTaller',            width:'180px',background:'#2196f3'},
    { title: 'Fecha Programada Entrega',    property: 'programacionEntrega',      width:'180px',background:'#2196f3'},
    { title: 'Apertura OT',                 property: 'aperturaOT',               width:'180px',background:'#2196f3'},
    { title: 'Fecha Inicio Mecánico',       property: 'inicioMecanico',           width:'180px',background:'#2196f3'},
    { title: 'Fecha Fin Mecánico',          property: 'finMecanico',              width:'180px',background:'#2196f3'},
    { title: 'Fecha Inicio Lavado',         property: 'inicioLavado',             width:'180px',background:'#2196f3'},
    { title: 'Fecha Fin Lavado',            property: 'finLavado',                width:'180px',background:'#2196f3'},
    { title: 'Fecha Llamada Notificación',  property: 'fechaNotificacion',        width:'185px',background:'#ffc107'},
    { title: 'Contesta/No Contesta',        property: 'contestaNoContesta',       width:'100px',background:'#ffc107'},
    { title: 'Fecha Nueva Entrega',         property: 'fechaEntrega',             width:'200px',background:'#ffc107'},
    { title: 'Voz Cliente',                 property: 'vozCliente',               width:'250px',background:'#ffc107'}
  ]
};

export interface ColumnDef {
  title: string;
  property: string;
  width?: string;
  align?: string;
}
