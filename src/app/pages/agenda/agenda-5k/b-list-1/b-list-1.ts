import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ExcelService } from 'app/pages/excel.service';
import { AgendaService } from '../../agenda.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'b-list-1',
  templateUrl: './b-list-1.html',
  styleUrls: ['./b-list-1.scss']
})
export class BList1 implements OnInit {
  displayedColumns: string[] = [
    'idPronostico5k',
    'placa',
    'modelo',
    'cliente',
    'telefono1',
    'telefono2',
    'mantenimiento',
    //'manttoToca',
    'nroPresupuesto',
    'recomendacion',
    'costo',
    'prioridad',
    'fecha15Antes',
    'fechaPactadaUltimoContacto',
    'fechaCorregida',
    'fechaCorregidaSistema',
    'fechaWppEnvio',
    'vistobueno',
    'resultado',
    'fechaLlamadaEfectuada',
    'contesta',
    'resultado2',
    'fechaPactadaVolverLlamar',
    'fechaCita'
    // 'fechaRegistro'
  ];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  loadingData: boolean;
  constructor(
    private service: AgendaService,
    private excelService: ExcelService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.actualizar();
  }
  buscar(placa) {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loadingData = true;
    this.service.listarAgenda5k().subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      _err => (this.loadingData = false),
      () => (this.loadingData = false)
    );
  }
  descargar() {
    this.excelService.exportAsExcelFile(
      this.dataSource.data,
      'Registro-Llamadas'
    );
  }

  buscarPorFecha(e) {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.loadingData = true;
    this.service.listarAgenda5kFecha(e.fechaInicio, e.fechaFin).subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
      },
      _err => (this.loadingData = false),
      () => (this.loadingData = false)
    );
  }

  actualizar() {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loadingData = true;
    this.service.listarAgenda5k().subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      _err => (this.loadingData = false),
      () => (this.loadingData = false)
    );

    this.loadingData = false;
  }
  filtrar(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selectRow(row) {
    sessionStorage.setItem('pronostico5k', JSON.stringify(row));
    this.router.navigate(['/llamar-cliente'], { relativeTo: this.route });
  }
}
