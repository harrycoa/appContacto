import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { AgendaService } from '../agenda.service';
import { ExcelService } from 'app/pages/excel.service';
import { Router } from '@angular/router';

@Component({
  selector: 'incumplimiento-cita',
  templateUrl: './incumplimiento-cita.html'
})
export class IncumplimientoCita implements OnInit {
  fecha = new Date();
  codigo = 0;

  loading;
  displayColumns: string[] = [];
  data: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  metadata = metadata;
  list = list;
  colores = colores;

  constructor(
    private api: AgendaService,
    private excel: ExcelService,
    private router: Router
  ) {}

  dayFilter = (d: Date): boolean => d.getDay() !== 0;

  ngOnInit() {
    this.displayColumns = this.metadata.columnDef.map(c => c.property);
    this.listar();
  }

  listar() {
    this.setData();
    this.loading = true;
    this.api.listarPostServicio(this.codigo, this.fecha).subscribe(
      res => {
        this.setData(res);
        this.loading = false;
      },
      _err => (this.loading = false)
    );
  }
  setData(data?: any) {
    this.data = new MatTableDataSource(data);
    this.data.paginator = this.paginator;
  }

  filtrar(value: string) {
    this.data.filter = value.trim().toLowerCase();
    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  descargar() {
    let datos = [];
    if (this.data.filteredData.length > 0) datos = this.data.filteredData;
    else datos = this.data.data;
    this.excel.exportAsExcelFile(datos, 'Notificación Termino Trabajo');
  }

  selectRow(e) {
    sessionStorage.setItem('agendas', e.placa);
    this.router.navigate(['/llamar-cliente']);
  }
}

const list = [
  { codigo: 1, nombre: 'Reclamo de Garantía (No SSC)' },
  { codigo: 2, nombre: 'FIR' },
  { codigo: 3, nombre: 'Mantenimiento Puro' },
  { codigo: 4, nombre: 'B & P' },
  { codigo: 5, nombre: 'Ofertas' },
  { codigo: 6, nombre: 'Reclamo de Garantía (SSC)' }
];

const colores = [
  '',
  '#c65911',
  '#ffc000',
  '#548235',
  '#2f75b5',
  '#7b7b7b',
  '#7030a0'
];

const metadata: any = {
  columnDef: [
    { width: '70px', title: 'Nro', property: 'nro' },
    { width: '70px', title: 'Placa', property: 'placa' },
    { width: '290px', title: 'Persona de Contacto', property: 'nombre' },
    { width: '90px', title: 'Teléfono 1', property: 'telefono' },
    { width: '90px', title: 'Teléfono 2', property: 'telefono2' },
    {
      width: '250px',
      title: 'Recomendación Mecánico',
      property: 'recomendacionMecanico'
    },
    {
      width: '180px',
      title: 'Recomendación Asesor Servicio',
      property: 'recomendacionAsesor'
    },
    { width: '180px', title: 'Hora Recepción', property: 'recepcion' },
    { width: '180px', title: 'Notificación TT', property: 'notificacionTT' },
    { width: '180px', title: 'Notificación HPE', property: 'notificacionHPE' },
    { width: '180px', title: 'Cierre OT', property: 'cierreOT' },
    { width: '180px', title: 'Facturación OT', property: 'facturacionOT' },
    { width: '180px', title: 'Resultado P1', property: 'p1' },
    { width: '185px', title: 'Resultado P2', property: 'p2' },
    { width: '100px', title: 'Resultado P3', property: 'p3' },
    { width: '250px', title: 'Voz Cliente', property: 'vozCliente' }
  ]
};
