import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { setUrl, toDateMySql } from '@shared/functions';

@Injectable()
export class AgendaService {
  constructor(private http: HttpClient) {}

  listarAgenda5k() {
    const URL = setUrl(`agenda5k`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  listarAgenda5kFecha(fechaInicial, fechaFinal) {
    const URL = setUrl(`agenda5k/${fechaInicial}/${fechaFinal}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
  //   listarAgenda5k(mclase) {
  //     const URL = setUrl(`agenda5k/${mclase}`);
  //     return this.http.get(URL).pipe(catchError(e => throwError(e)));
  //   }

  listarNotificacionTF(fecha: Date) {
    const f = toDateMySql(fecha.toLocaleDateString());
    const URL = setUrl(`anotificacion/${f}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarPostServicio(codigoServicio, fecha: Date) {
    const f = toDateMySql(fecha.toLocaleDateString());
    const URL = setUrl(`apostservicio/${codigoServicio}/${f}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarSSC(codigo, fecha: Date) {
    const f = toDateMySql(fecha.toLocaleDateString());
    const URL = setUrl(`assc/${codigo}/${f}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarGestionNegativa(fecha: Date) {
    const f = toDateMySql(fecha.toLocaleDateString());
    const URL = setUrl(`agestionnegativa/${f}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }

  listarIncumplimientoCita(codigo, fecha: Date) {
    const f = toDateMySql(fecha.toLocaleDateString());
    const URL = setUrl(`aincumplimiento/${codigo}/${f}`);
    return this.http.get(URL).pipe(catchError(e => throwError(e)));
  }
}
