import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TiposService } from 'app/pages/tipos.service';
import { tap } from 'rxjs/operators';
import { timeTo24h } from '@shared/functions';

@Component({
  selector: 'motivo-estado',
  templateUrl: './motivo-estado.component.html',
  styleUrls: ['./motivo-estado.component.scss']
})
export class MotivoEstadoComponent implements OnInit {
  form: FormGroup;
  motivos;
  estados;
  _estados;
  loadingTipos: boolean;
  horaRetomarContacto;

  @Output()
  changeMotivoEstado = new EventEmitter<{ idMotivo; seLogroContacto }>();

  constructor(private fb: FormBuilder, private service: TiposService) {}

  dayFilter = (d: Date): boolean => {
    const day = d.getDay();
    return day !== 0; // && day !== 6;
    // tslint:disable-next-line:semicolon
  };

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    this.horaRetomarContacto = '12:00 PM';

    this.loadingTipos = true;
    this.getMotivos().subscribe(
      _res1 =>
        this.getEstados().subscribe(
          _res2 => (this.loadingTipos = false),
          _err => (this.loadingTipos = false)
        ),
      _err => (this.loadingTipos = false)
    );
    this.form = this.fb.group(this.initForm());
    this.changeMotivo();
    this.changeEstadoLlamada();
  }

  reset() {
    this.horaRetomarContacto = '12:00 PM';
    this.form.reset();
  }

  initForm() {
    return {
      idMotivo: [null, Validators.required],
      manttoK: [null, Validators.required],
      seLogroContacto: [null, Validators.required],
      idEstado: [null, Validators.required],
      ultimoKilometrajeServicio: null,
      vozCliente: null,
      fechaRetomarContacto: null,
      nuevoDuenio: null,
      telefonoNuevoDuenio: null
    };
  }
  resetForm() {
    this.horaRetomarContacto = '12:00 PM';
    return {
      idMotivo: null,
      manttoK: null,
      seLogroContacto: null,
      idEstado: null,
      ultimoKilometrajeServicio: null,
      vozCliente: null,
      fechaRetomarContacto: null,
      nuevoDuenio: null,
      telefonoNuevoDuenio: null
    };
  }

  getMotivos() {
    return this.service
      .listarMotivos('B')
      .pipe(tap(res => (this.motivos = res)));
  }
  getEstados() {
    return this.service.listarEstados().pipe(tap(res => (this.estados = res)));
  }

  changeMotivo() {
    this.form.get('idMotivo').valueChanges.subscribe(idMotivo => {
      // idMotivo=18 => B4.INVITACION A 5K-150K
      if (idMotivo == 18) {
        this.form.get('manttoK').enable();
      } else {
        this.form.get('manttoK').disable();
        this.form.patchValue({ manttoK: null });
      }
      if (idMotivo > 17 && this.form.get('seLogroContacto').value == 'S') {
        this.form.get('ultimoKilometrajeServicio').enable();
      } else this.form.get('ultimoKilometrajeServicio').disable();

      const seLogroContacto = this.form.get('seLogroContacto').value;
      this.changeMotivoEstado.emit({ idMotivo, seLogroContacto });
    });
  }

  changeEstadoLlamada() {
    this.form.get('seLogroContacto').valueChanges.subscribe(seLogroContacto => {
      const filtro = seLogroContacto == 'S' ? 'A' : 'B';
      this._estados = this.estados.filter(e => e.eClase == filtro);

      if (seLogroContacto == 'S' && this.form.get('idMotivo').value > 17) {
        this.form.get('ultimoKilometrajeServicio').enable();
      } else {
        this.form.get('ultimoKilometrajeServicio').disable();
      }

      const idMotivo = this.form.get('idMotivo').value;
      this.changeMotivoEstado.emit({ idMotivo, seLogroContacto });
    });
  }

  changeFecha(fecha) {
    this.setFechaHoraRetomar();
  }
  changeHora(hora) {
    this.horaRetomarContacto = hora;
    if (this.form.get('fechaRetomarContacto').value != null)
      this.setFechaHoraRetomar();
  }

  setFechaHoraRetomar() {
    const h = timeTo24h(this.horaRetomarContacto);
    const fechaRetomarContacto: Date = this.form.get('fechaRetomarContacto')
      .value as Date;
    fechaRetomarContacto.setHours(h.horas, Number(h.minutos), 0);
    this.form.patchValue({ fechaRetomarContacto });
  }
}
