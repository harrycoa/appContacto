import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'agendar-cita',
  templateUrl: './agendar-cita.component.html',
  styleUrls: ['./agendar-cita.component.scss']
})
export class AgendarCitaComponent implements OnInit {
  @Output()
  change = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  changeValue(value) {
    this.change.emit(value);
  }
}
