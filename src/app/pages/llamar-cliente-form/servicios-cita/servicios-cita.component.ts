import { Component, OnInit, ViewChild } from '@angular/core';
import { MatCheckbox } from '@angular/material';

@Component({
  selector: 'servicios-cita',
  templateUrl: './servicios-cita.component.html',
  styleUrls: ['./servicios-cita.component.scss']
})
export class ServiciosCitaComponent implements OnInit {
  _servicios: Servicio[] = [];
  campanias = CAMPANIAS;

  mantenimiento: string;
  diagnostico: string;
  reparacion: string;
  campania: string;
  byp: string;
  garantia: string;
  ssc: string;
  insAccesorio: string;
  otro: string;

  @ViewChild('chkMantto')
  chkMantto: MatCheckbox;
  @ViewChild('chkDiagnostico')
  chkDiagnostico: MatCheckbox;
  @ViewChild('chkReparacion')
  chkReparacion: MatCheckbox;
  @ViewChild('chkCampana')
  chkCampana: MatCheckbox;
  @ViewChild('chkByP')
  chkByP: MatCheckbox;
  @ViewChild('chkGarantia')
  chkGarantia: MatCheckbox;
  @ViewChild('chkSsc')
  chkSsc: MatCheckbox;
  @ViewChild('chkInsAccesorio')
  chkInsAccesorio: MatCheckbox;
  @ViewChild('chkOtro')
  chkOtro: MatCheckbox;

  constructor() {}

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    this.mantenimiento = '0';
  }

  changeServicio(checked: boolean, detalle: string) {
    const s: Servicio = {
      detalle,
      detalleDescripcion: null
    };

    if (checked) {
      this._servicios.push(s);
    } else {
      const index = this._servicios.map(i => i.detalle).indexOf(detalle);
      this._servicios.splice(index, 1);
    }
  }

  servicios(): Servicio[] {
    this._servicios.forEach(c => {
      let detalleDescripcion = '';
      switch (c.detalle) {
        case 'Mantenimiento':
          detalleDescripcion = this.mantenimiento;
          break;
        case 'Diagnóstico':
          detalleDescripcion = this.diagnostico;
          break;
        case 'Reparación':
          detalleDescripcion = this.reparacion;
          break;
        case 'Campaña':
          detalleDescripcion = this.campania;
          break;
        case 'B & P':
          detalleDescripcion = this.byp;
          break;
        case 'Garantía':
          detalleDescripcion = this.garantia;
          break;
        case 'SSC':
          detalleDescripcion = this.ssc;
          break;
        case 'Instalación Accesorio':
          detalleDescripcion = this.insAccesorio;
          break;
        case 'Otro':
          detalleDescripcion = this.otro;
          break;
      }
      c.detalleDescripcion =
        detalleDescripcion != null
          ? detalleDescripcion.toString()
          : detalleDescripcion;
    });
    return this._servicios;
  }
}

const CAMPANIAS = [
  'CAMPAÑA 1',
  'CAMPAÑA 2',
  'CAMPAÑA 3',
  'CAMPAÑA 4',
  'CAMPAÑA 5'
];

export interface Servicio {
  detalle: string;
  detalleDescripcion: string;
}
