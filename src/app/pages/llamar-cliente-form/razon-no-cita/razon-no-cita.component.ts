import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'razon-no-cita',
  templateUrl: './razon-no-cita.component.html',
  styleUrls: ['./razon-no-cita.component.scss']
})
export class RazonNoCitaComponent implements OnInit {
  @Input()
  idMotivo: number;

  razones: string[] = [];

  @Output()
  change = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {
    this.getRazones(this.idMotivo);
  }

  getRazones(idMotivo) {
    switch (idMotivo) {
      case 17:
      case 18:
      case 20:
        this.razones = razones171820;
        break;
      case 19:
        this.razones = razones19;
        break;
      case 21:
        this.razones = razones21;
        break;
    }
  }

  changeRazon(razonNoCita) {
    this.change.emit(razonNoCita);
  }
}

const razones171820 = [
  'NO RECIBIÓ TARJETA NI PLACA',
  'NO RECIBIÓ PLACA',
  'FALTA RECORRIDO',
  'DESCONOCE DATOS',
  'REALIZÓ MANTTO EN OTRO CONCESIONARIO',
  'REALIZÓ MANTTO EN OTRO TALLER',
  'NO DESEA CITA',
  'NO DESEA ATENCIÓN EN CORASUR',
  'NO LE INTERESA LA CAMPAÑA',
  'OTROS'
];

const razones19 = [
  'WHATSAPP NO CONTESTADO',
  'LA UNIDAD OPERA FUERA DE CUSCO',
  'NO DESEA REALIZAR SSC',
  'REALIZÓ EL SERVICIO EN OTRO CONCESIONARIO',
  'REQUIERE MAYOR INFORMACIÓN SOBRE EL SSC',
  'OTRO'
];

const razones21 = [
  'WHATSAPP NO CONTESTADO',
  'LA UNIDAD OPERA FUERA DE CUSCO',
  'NO DESEA RETORNAR',
  'REALIZÓ EL SERVICIO EN OTRO CONCESIONARIO',
  'REQUIERE MAYOR INFORMACIÓN',
  'OTRO'
];
