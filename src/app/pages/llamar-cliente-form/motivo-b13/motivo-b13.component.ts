import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'motivo-b13',
  templateUrl: './motivo-b13.component.html',
  styleUrls: ['./motivo-b13.component.scss']
})
export class MotivoB13Component implements OnInit {
  form: FormGroup;
  postergaciones = POSTERGACIONES;
  label = '';

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      motivoDescripcion: [null, Validators.required],
      fechaCitaReprogramada: [null, Validators.required],
      vozCliente: [null, Validators.required]
    });
  }
  changeSelect(value) {
    switch (value) {
      case 'Acepta la postergación conforme con la nueva fecha y/o hora propuesta para la entrega':
        this.label = 'Motivo de aceptación conforme';
        break;
      case 'Acepta la postergación inconforme  con la nueva fecha y/o hora propuesta para la entrega':
        this.label = 'Voz Cliente Negativa';
        break;
      case 'Propone conforme una alternativa de fecha y/o hora de entrega':
        this.label = 'Motivo de cambio de fecha de entrega';
        break;
      case 'Propone inconforme una alternativa de fecha y/o hora de entrega':
        this.label = 'Voz Cliente Negativa';
        break;
    }
  }
}

const POSTERGACIONES = [
  'Acepta la postergación conforme con la nueva fecha y/o hora propuesta para la entrega',
  'Acepta la postergación inconforme  con la nueva fecha y/o hora propuesta para la entrega',
  'Propone conforme una alternativa de fecha y/o hora de entrega',
  'Propone inconforme una alternativa de fecha y/o hora de entrega'
];
