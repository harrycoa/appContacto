import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  ServiciosCitaComponent,
  Servicio
} from '../servicios-cita/servicios-cita.component';

@Component({
  selector: 'motivo-b5',
  templateUrl: './motivo-b5.component.html',
  styleUrls: ['./motivo-b5.component.scss']
})
export class MotivoB5Component implements OnInit {
  titulo = '';
  form: FormGroup;

  private _idMotivo: number;
  @Input()
  set idMotivo(id) {
    this._idMotivo = id;
    this.setTitle(id);
  }
  get idMotivo() {
    return this._idMotivo;
  }

  vozCliente = null;

  @ViewChild('sC')
  sC: ServiciosCitaComponent;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }
  setTitle(idMotivo) {
    switch (idMotivo) {
      case 19:
        this.titulo = 'B5. INVITACION A SSC';
        break;
      case 21:
        this.titulo = 'B7. INVITACION A RETORNO';
        break;
    }
  }

  initForm() {
    this.form = this.fb.group({
      agendaCita: ['S', Validators.required],
      fechaCita: [null, Validators.required],
      razonNoCita: [null, Validators.required],
      vozCliente: [null, Validators.required]
    });
  }

  reset() {
    this.vozCliente = null;
    this.form.reset({
      agendaCita: 'S',
      fechaCita: null,
      razonNoCita: null,
      vozCliente: null
    });
  }

  changeAgendarCita(agendaCita) {
    this.form.patchValue({ agendaCita });
    this.form.patchValue({ fechaCita: null });
    this.form.patchValue({ razonNoCita: null });
    this.form.patchValue({ vozCliente: null });
    this.vozCliente = null;
  }

  changeFecha(fechaCita) {
    this.form.patchValue({ fechaCita });
  }

  changeRazonNoCita(razonNoCita) {
    this.form.patchValue({ razonNoCita });
  }

  changeVozCliente() {
    this.form.patchValue({ vozCliente: this.vozCliente });
  }

  valueForm() {
    let form = { ...this.form.value, detalle: [] };
    if (this.form.get('agendaCita').value == 'S')
      form.detalle = this.sC.servicios();
    return form;
  }
}
