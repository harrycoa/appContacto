import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'motivo-b11',
  templateUrl: './motivo-b11.component.html',
  styleUrls: ['./motivo-b11.component.scss']
})
export class MotivoB11Component implements OnInit {
  form: FormGroup;
  aprobaciones = APROBACIONES;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      motivoDescripcion: [null],
      numeroOt: [null],
      razonNoCita: [null],
      aFreno: [null],
      fechaCitaCambiada: [null],
      vozCliente: [null]
    });
  }

  changeSelect() {
    this.form.patchValue({
      numeroOt: null,
      razonNoCita: null,
      aFreno: null,
      fechaCitaCambiada: null,
      vozCliente: null
    });
  }
}

export const APROBACIONES = [
  'Acepta la reparación adicional conforme con la nueva fecha propuesta',
  'Acepta la reparación adicional inconforme con la nueva fecha propuesta',
  'No acepta la reparación adicional por motivos propios',
  'No acepta la reparación adicional inconforme'
];
