import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'motivo-b2',
  templateUrl: './motivo-b2.component.html',
  styleUrls: ['./motivo-b2.component.scss']
})
export class MotivoB2Component implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder) {}

  dayFilter = (d: Date) => d.getDay() !== 0;

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    this.form = this.fb.group(this.initForm());
  }
  reset() {
    this.form.reset();
  }

  initForm() {
    return {
      motivoDescripcion: [null, Validators.required],
      estadoContacto: ['P', Validators.required],
      fechaRecojoDocumento: [null, Validators.required],
      vozCliente: [null, Validators.required]
    };
  }
  resetForm() {
    return {
      motivoDescripcion: null,
      estadoRecojo: 'P',
      fechaRecojoDocumento: null,
      vozCliente: null
    };
  }
}
