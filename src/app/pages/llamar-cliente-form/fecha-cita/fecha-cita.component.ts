import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { timeTo24h } from '@shared/functions';

@Component({
  selector: 'fecha-cita',
  templateUrl: './fecha-cita.component.html',
  styleUrls: ['./fecha-cita.component.scss']
})
export class FechaCitaComponent implements OnInit {
  fechaCita;
  horaCita = '12:00 PM';

  @Output()
  change = new EventEmitter<any>();

  constructor() {}
  dayFilter = (d: Date) => d.getDay() !== 0;

  ngOnInit() {
    this.fechaCita = null;
  }

  changeFecha(fecha) {
    this.fechaCita = fecha;

    this.setFechaHora();
    this.change.emit(this.fechaCita);
  }

  changeHora(hora) {
    this.horaCita = hora;

    if (this.fechaCita != null) this.setFechaHora();
    this.change.emit(this.fechaCita);
  }

  setFechaHora() {
    const h = timeTo24h(this.horaCita);
    (this.fechaCita as Date).setHours(h.horas, Number(h.minutos), 0);
  }
}
