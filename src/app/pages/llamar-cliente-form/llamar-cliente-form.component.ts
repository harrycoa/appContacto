import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';

import { DatosBasicosComponent } from './datos-basicos/datos-basicos.component';
import { MotivoEstadoComponent } from './motivo-estado/motivo-estado.component';

import { FormPrincipal } from '@models/form-principal';

@Component({
  selector: 'llamar-cliente-form',
  templateUrl: './llamar-cliente-form.component.html',
  styleUrls: ['./llamar-cliente-form.component.scss']
})
export class LlamarClienteFormComponent implements OnInit {
  @Output()
  enviar = new EventEmitter<any>();

  formulario: FormPrincipal;

  //#region COMPONENTES MOTIVOS

  @ViewChild('datosBasicos')
  datosBasicos: DatosBasicosComponent;

  @ViewChild('motivoEstado')
  motivoEstado: MotivoEstadoComponent;
  idMotivo;
  seLogroContacto;

  @ViewChild('formMotivo')
  formMotivo: any;
  //#endregion

  constructor() {}

  ngOnInit() {
    this.formulario = new FormPrincipal();
  }

  guardar() {
    this.setForm();
    // console.log(this.formulario);
    this.enviar.emit(this.formulario);
  }

  changeMotivoActivo(e) {
    this.idMotivo = e.idMotivo;
    this.seLogroContacto = e.seLogroContacto;
    this.formulario = new FormPrincipal();
  }

  setForm() {
    this.setDatosBasicos();
    this.setMotivoEstado();
    if (this.seLogroContacto == 'S') this.setMotivos();
  }

  setDatosBasicos() {
    const f = this.datosBasicos.form.value;
    this.formulario = { ...this.formulario, ...f };
    // this.formulario.pronostico5k = {
    //   idPronostico5k: db.idPronostico,
    //   idPronostico: db.idPronostico
    // };
  }
  setMotivoEstado() {
    const f = this.motivoEstado.form.value;
    this.formulario = { ...this.formulario, ...f };
  }

  setMotivos() {
    switch (this.idMotivo) {
      case 15: // B1
      case 16: // B2
      case 22: // B8
      case 23: // B9
      case 24: // B10
      case 25: // B11
      case 26: // B12
      case 27: // B13
      case 28: // B14
      case 29: // B15
      case 30: // B16
        this.motivoB1B2();
        break;
      case 17: // B3
      case 18: // B4
      case 19: // B5
      case 20: // B6
      case 21: // B7
        this.motivoB3B7();
        break;
      default:
        break;
    }
  }
  reset() {
    this.datosBasicos.reset();
    this.motivoEstado.reset();
    // this.formMotivo.reset();
  }
  motivoB1B2() {
    //motivos B8, B9, B10, B11, B12, B13. B14, B15, B16
    const f = this.formMotivo.form.value;
    this.formulario = { ...this.formulario, ...f };
  }
  motivoB3B7() {
    const f = this.formMotivo.valueForm();
    this.formulario = { ...this.formulario, ...f };
  }
}
