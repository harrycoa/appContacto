import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatInput } from '@angular/material';

import { Usuario } from '@core/auth/usuario';
import { AuthService } from '@core/auth/auth.service';
import { TiposService } from 'app/pages/tipos.service';
import { OrdenReparacion } from '@models/ordenReparacion';

import { DialogOrdenComponent } from 'app/pages/dialog-orden/dialog-orden.component';
import { SeriePlaca } from '@models/serie-placa';
import { HistorialContactoDialog } from 'app/pages/historial-contacto-page/historial-contacto-dialog.component';

@Component({
  selector: 'datos-basicos',
  templateUrl: './datos-basicos.component.html',
  styleUrls: ['./datos-basicos.component.scss']
})
export class DatosBasicosComponent implements OnInit {
  form: FormGroup;
  usuario: Usuario;
  pendienteBuscar: boolean;
  ordenesReparacion: OrdenReparacion[];
  seriePlaca: SeriePlaca;

  inputSP = '';

  constructor(
    private auth: AuthService,
    private fb: FormBuilder,
    private service: TiposService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    this.usuario = this.auth.getUser();
    this.ordenesReparacion = [];
    this.seriePlaca = new SeriePlaca();

    this.initForm();

    this.changeTipoContactoT();
    this.changeTipoContactoC();
    this.desdePronostico5k();
    this.desdeAgendas();
  }

  desdePronostico5k() {
    if (sessionStorage.getItem('pronostico5k') != null) {
      const data = JSON.parse(sessionStorage.getItem('pronostico5k'));
      this.form.patchValue({
        placa: data.placa,
        idPronostico: data.idPronostico5k
      });
      this.searchPlacaSerie(this.form.get('placa').value);
      sessionStorage.removeItem('pronostico5k');
    }
  }
  desdeAgendas() {
    if (sessionStorage.getItem('agendas') != null) {
      const placa = sessionStorage.getItem('agendas');
      this.form.patchValue({ placa });
      this.searchPlacaSerie(placa);
      sessionStorage.removeItem('agendas');
    }
  }

  //#region FORMULARIO
  reset() {
    this.resetForm();
    this.inputSP = '';
  }

  initForm() {
    this.form = this.fb.group({
      placa: [null],
      serie: [null],
      motor: [null],
      chasis: [null],
      vin: [null],
      anioFabricacion: [null],
      modelo: [null, Validators.required],
      trabajadorCorasur: [this.usuario.nombreUsuario],
      tipoContactoCorasur: ['Teléfono', Validators.required],
      contactoCorasur: [this.usuario.telefono, Validators.required],
      cliente: [null, Validators.required],
      tipoContactoCliente: ['Teléfono', Validators.required],
      contactoCliente: [null, Validators.required],
      telefono2: [null]
    });
  }
  resetForm() {
    this.seriePlaca = new SeriePlaca();
    this.form.reset({
      placa: null,
      serie: null,
      motor: null,
      chasis: null,
      vin: null,
      anioFabricacion: null,
      modelo: null,
      trabajadorCorasur: this.usuario.nombreUsuario,
      tipoContactoCorasur: 'Teléfono',
      contactoCorasur: this.usuario.telefono,
      cliente: null,
      tipoContactoCliente: 'Teléfono',
      contactoCliente: null,
      telefono2: null
    });
  }

  changeTipoContactoT() {
    this.form.get('tipoContactoCorasur').valueChanges.subscribe(value => {
      if (value == 'Email') {
        this.form.patchValue({ contactoCorasur: this.usuario.email });
      } else this.form.patchValue({ contactoCorasur: this.usuario.telefono });
    });
  }
  changeTipoContactoC() {
    this.form.get('tipoContactoCliente').valueChanges.subscribe(value => {
      switch (value) {
        case 'Teléfono':
          this.form.patchValue({
            contactoCliente: this.seriePlaca.telefono
          });
          break;
        default:
          this.form.patchValue({ contactoCliente: '' });
          break;
      }
    });
  }
  onBlur(nameControl) {
    this.form.get(nameControl).markAsUntouched();
  }

  //#endregion

  //#region MODAL ORDEN REPARACION

  buscarSeriePlaca(valuePlacaSerie, inputModelo, e: Event) {
    e.stopPropagation();
    inputModelo.focus();
    this.searchPlacaSerie(valuePlacaSerie);
  }

  buscarOrdenServicio(valuePlacaSerie, inputModelo, e: Event) {
    e.stopPropagation();
    inputModelo.focus();
    this.buscarOrden(valuePlacaSerie);
  }

  buscarOrden(placa) {
    this.pendienteBuscar = true;
    this.service.buscarOrden(placa).subscribe(
      (ordenes: OrdenReparacion[]) => {
        this.pendienteBuscar = false;
        this.ordenesReparacion = ordenes;
        this.showDialog(this.ordenesReparacion);
      },
      _err => (this.pendienteBuscar = false)
    );
  }

  showDialog(data) {
    const options = {
      data,
      maxheight: '90%',
      width: '94%'
    };
    this.dialog.open(DialogOrdenComponent, options);
  }

  searchPlacaSerie(value) {
    this.pendienteBuscar = true;
    this.service.buscarSeriePlaca(value).subscribe(
      (res: SeriePlaca) => {
        this.pendienteBuscar = false;
        this.seriePlaca = new SeriePlaca();
        this.seriePlaca = res;
        this.form.patchValue({
          serie: res.serie,
          placa: res.placa,
          motor: res.motor,
          chasis: res.chasis,
          vin: res.vin,
          anioFabricacion: res.anioFabricacion,
          cliente: res.cliente,
          contactoCliente: res.telefono,
          modelo: res.modelo,
          telefono2: res.telefono2
        });
      },
      _err => (this.pendienteBuscar = false)
    );
  }
  //#endregion
  verHistorialContactos(valuePlacaSerie, selModelo, e: Event) {
    e.stopPropagation();
    selModelo.focus();
    const options = {
      data: valuePlacaSerie,
      maxheight: '90%',
      width: '94%'
    };
    this.dialog.open(HistorialContactoDialog, options);
  }
}
