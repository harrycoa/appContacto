import { Component, OnInit, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Pregunta } from '../motivo-b14/motivo-b14.component';

@Component({
  selector: 'encuesta',
  templateUrl: './encuesta.component.html'
})
export class EncuestaComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<EncuestaComponent>,
    @Inject(MAT_DIALOG_DATA) public preguntas
  ) {}

  ngOnInit() {}

  changeEstado(checked, index: number) {
    if (checked) {
      if (this.preguntas[index].tieneEstado) {
        this.preguntas[index].label = 'Voz Cliente Positiva';
        this.preguntas[index].placeHolder = 'Ingrese Voz Cliente Positiva';
      }
      if (index + 1 < this.preguntas.length)
        this.preguntas[index + 1].disabled = false;
    } else {
      if (this.preguntas[index].tieneEstado) {
        this.preguntas[index].label = 'Voz Cliente Negativa';
        this.preguntas[index].placeHolder = 'Ingrese Voz Cliente Negativa';
      }
      for (let i = index + 1; i < this.preguntas.length; i++) {
        this.preguntas[i].disabled = true;
        this.preguntas[i].estado = false;
        this.preguntas[i].detalleDescripcion = '';
        if (this.preguntas[i].tieneEstado) {
          this.preguntas[i].label = 'Voz Cliente Negativa';
          this.preguntas[i].placeHolder = 'Ingrese Voz Cliente Negativa';
        }
      }
    }
  }
}
