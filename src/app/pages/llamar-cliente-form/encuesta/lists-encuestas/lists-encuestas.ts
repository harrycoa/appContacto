export const ListEncuestas = [
    {idEncuesta:1,encuesta: '¿SU VEHICULO FUE REPARADO EN LA PRIMERA OPORTUNIDAD?'},
    {idEncuesta:2,encuesta: '¿SE CUMPLIO CON LA FECHA Y HORA PROMETIDA PARA ENTREGA?'},
    {idEncuesta:3,encuesta: '¿SE ENCUENTRA SATISFECHO CON LA DURACION DEL SERVICIO?'},
    {idEncuesta:4,encuesta: '¿SE ENCUENTRA SATISFECHO CON EL SERVICIO REALIZADO?'},
    {idEncuesta:5,encuesta: '¿SU VEHICULO FUE ENTREGADO A LA HORA PROGRAMADA?'},
    {idEncuesta:6,encuesta: '¿EL VEHICULO FUE ENTREGADO LIMPIO?'},
    {idEncuesta:7,encuesta: '¿TIENE ALGUNA SUGERENCIA QUE NOS PERMITA MEJORAR EL SERVICIO?'}
  ];