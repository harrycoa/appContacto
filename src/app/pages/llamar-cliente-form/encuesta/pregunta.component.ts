import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from '@angular/core';

@Component({
  selector: 'pregunta',
  templateUrl: './pregunta.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      .pregunta {
        font-weight: bold;
      }
    `
  ]
})
export class PreguntaComponent implements OnInit {
  @Input() disabled: boolean;
  @Input() detalle: string;
  private _detalleDescripcion: string;
  @Input()
  set detalleDescripcion(descripcion: string) {
    this._detalleDescripcion = descripcion;
  }
  get detalleDescripcion() {
    return this._detalleDescripcion;
  }

  private _estado: boolean;
  @Input()
  set estado(estado: boolean) {
    this._estado = estado;
  }
  get estado() {
    return this._estado;
  }
  @Input() tieneEstado: boolean;

  @Input() label: string;
  @Input() placeHolder: string;

  @Output() changeEstado = new EventEmitter<boolean>();
  @Output() changeDescripcion = new EventEmitter<string>();

  constructor() {}

  changeState(value) {
    this.changeEstado.emit(value);
  }

  changeInput(val) {
    this.changeDescripcion.emit(val);
  }

  ngOnInit() {}
}
