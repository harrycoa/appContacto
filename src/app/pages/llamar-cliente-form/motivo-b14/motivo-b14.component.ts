import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatDialog } from '@angular/material';
import { EncuestaComponent } from './../encuesta/encuesta.component';

@Component({
  selector: 'motivo-b14',
  templateUrl: './motivo-b14.component.html',
  styleUrls: ['./motivo-b14.component.scss']
})
export class MotivoB14Component implements OnInit {
  form: FormGroup;
  servicios = SERVICIOS;

  preguntas: Pregunta[] = [];

  constructor(private fb: FormBuilder, private dialog: MatDialog) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      motivoDescripcion: [null, Validators.required],
      detalle: [[]]
    });
  }
  resetForm() {
    this.form.reset({
      motivoDescripcion: null,
      detalle: []
    });
  }

  changeServicio() {
    switch (this.form.get('motivoDescripcion').value) {
      case 'FIR':
      case 'Garantía':
        this.preguntas = JSON.parse(JSON.stringify(preguntasA));
        break;

      case 'Mantenimiento Puro':
      case 'B & P':
      case 'Instalación de Accesorios':
        this.preguntas = JSON.parse(JSON.stringify(preguntasB));
        break;
      default:
        this.preguntas = [];
        break;
    }
    if (this.form.get('motivoDescripcion').value != null) this.abrirEncuesta();
  }
  abrirEncuesta() {
    const dialogRef = this.dialog.open(EncuestaComponent, {
      maxWidth: '650px',
      minWidth: '320px',
      disableClose: true,
      data: this.preguntas
    });

    dialogRef.afterClosed().subscribe(() => {
      const detalle = this.preguntas.map(p => {
        const estado = p.estado ? 'S' : 'N';
        const detalle = p.detalle;
        const detalleDescripcion = p.detalleDescripcion;
        const d = { detalle, detalleDescripcion, estado };
        return d;
      });
      this.form.patchValue({ detalle });
    });
  }
  borrarRespuestas() {
    this.form.patchValue({ motivoDescripcion: null, detalle: [] });
    this.preguntas = [];
  }
}

export const SERVICIOS = [
  'FIR',
  'Mantenimiento Puro',
  'B & P',
  'Garantía',
  'Instalación de Accesorios'
];

const preguntasA: Pregunta[] = [
  {
    disabled: false,
    detalle: '¿Su vehículo fue reparado en la primera oportunidad?',
    detalleDescripcion: '',
    tieneEstado: true,
    estado: true,
    label: 'Voz Cliente Positiva',
    placeHolder: 'Ingrese Voz Cliente Positiva'
  },
  {
    disabled: false,
    detalle: '¿Se cumplió con la fecha y hora prometida para entrega?',
    detalleDescripcion: '',
    tieneEstado: true,
    estado: true,
    label: 'Voz Cliente Positiva',
    placeHolder: 'Ingrese Voz Cliente Positiva'
  },
  {
    disabled: false,
    detalle: '¿Se encuentra satisfecho con la duración del servicio?',
    detalleDescripcion: '',
    tieneEstado: true,
    estado: true,
    label: 'Voz Cliente Positiva',
    placeHolder: 'Ingrese Voz Cliente Positiva'
  }
];

const preguntasB: Pregunta[] = [
  {
    disabled: false,
    detalle: '¿Se encuentra satisfecho con el servicio realizado?',
    detalleDescripcion: '',
    tieneEstado: true,
    estado: true,
    label: 'Voz Cliente Positiva',
    placeHolder: 'Ingrese Voz Cliente Positiva'
  },
  {
    disabled: false,
    detalle: '¿Su vehículo fue entregado a la hora programada?',
    detalleDescripcion: '',
    tieneEstado: true,
    estado: true,
    label: 'Voz Cliente Positiva',
    placeHolder: 'Ingrese Voz Cliente Positiva'
  },
  {
    disabled: false,
    detalle: '¿El vehículo fue entregado limpio?',
    detalleDescripcion: '',
    tieneEstado: true,
    estado: true,
    label: 'Voz Cliente Positiva',
    placeHolder: 'Ingrese Voz Cliente Positiva'
  },
  {
    disabled: false,
    detalle: '¿Tiene alguna sugerencia que nos permita mejorar el servicio?',
    detalleDescripcion: '',
    tieneEstado: false,
    estado: false,
    label: 'Sugerencia',
    placeHolder: 'Ingrese Sugerencia'
  }
];

export interface Pregunta {
  disabled: boolean;
  detalle: string;
  detalleDescripcion: string;
  tieneEstado: boolean;
  estado: boolean;
  label: string;
  placeHolder: string;
}
