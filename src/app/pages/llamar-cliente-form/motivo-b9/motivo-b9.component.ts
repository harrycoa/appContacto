import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { timeTo24h } from '@shared/functions';

@Component({
  selector: 'motivo-b9',
  templateUrl: './motivo-b9.component.html',
  styleUrls: ['./motivo-b9.component.scss']
})
export class MotivoB9Component implements OnInit {
  form: FormGroup;
  recordatorios = RECORDATORIOS;
  razonesCambioCita = RAZONES_CAMBIO_CITA;
  motivosCancelaCita = MOTIVOS_CANCELA_CITA;

  constructor(private fb: FormBuilder) {}

  dayFilter = (d: Date) => d.getDay() != 0;

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      motivoDescripcion: [null],
      fechaCita: [null],
      vozCliente: [null],
      personaModificaCita: [null],
      razonModificaCita: [null],
      razonNoCita: [null],
      horaPosibleArribo: [null],
      fechaSugeridaClienteProxLlamada: [null]
    });
  }

  setHora(hora) {
    const h = timeTo24h(hora);
    let date = new Date();
    date.setHours(h.horas, Number(h.minutos), 0);
    return date;
  }
}

export const RECORDATORIOS = [
  'Confirma la cita',
  'Requiere cambios en la cita',
  'Mensaje de voz con el recordatorio de cita',
  'Mensaje de texto con el recordatorio de cita',
  'Mensaje de Whatsapp con el recordatorio de cita',
  'Confusión al reservar cita',
  'Cancelada'
];

export const RAZONES_CAMBIO_CITA = [
  'Cambio tipo de servicio',
  'Cambio de hora mismo día',
  'Cambio fecha y hora',
  'Otro'
];

export const MOTIVOS_CANCELA_CITA = [
  'Falta Repuesto',
  'Salud',
  'Trabajo',
  'Viaje',
  'Motivos Económicos',
  'No Desea',
  'Mala Atención'
];
