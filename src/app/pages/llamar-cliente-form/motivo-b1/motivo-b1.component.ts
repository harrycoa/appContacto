import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'motivo-b1',
  templateUrl: './motivo-b1.component.html',
  styleUrls: ['./motivo-b1.component.scss']
})
export class MotivoB1Component implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    this.form = this.fb.group(this.initForm());
  }

  reset() {
    this.form.reset();
  }

  initForm() {
    return {
      estadoContacto: ['P', Validators.required],
      vozCliente: [null, Validators.required]
    };
  }
  resetForm() {
    return {
      estadoContacto: 'P',
      vozCliente: null
    };
  }
}
