import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { timeTo24h } from '@shared/functions';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'motivo-b16',
  templateUrl: './motivo-b16.component.html',
  styleUrls: ['./motivo-b16.component.scss']
})
export class MotivoB16Component implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }

  reset() {
    this.form.reset();
  }
  initForm() {
    this.form = this.fb.group({
      numeroOt: [null, Validators.required],
      motivoDescripcion: [null, Validators.required],
      personaModificaCita: [null, Validators.required],
      nuevoDuenio: [null, Validators.required],
      telefonoNuevoDuenio: [null, Validators.required],
      horaInicio: [null, Validators.required],
      horaEntrega: [null, Validators.required],
      horaPosibleArribo: [null, Validators.required],
      fechaPactadaProxLlamada: [null, Validators.required],
      razonModificaCita: [null, Validators.required],
      detalleCita: [null, Validators.required],
      razonNoCita: [null, Validators.required],
      resumenContacto: [null, Validators.required],
      fechaCitaCambiada: [null, Validators.required],
      fechaCitaReprogramada: [null, Validators.required],
      aFreno: [null, Validators.required],
      bFreno: [null, Validators.required],
      cFreno: [null, Validators.required],
      dFreno: [null, Validators.required],
      usoVehicular: [null, Validators.required],
      vozCliente: [null, Validators.required],
      kmActual: [null, Validators.required],
      fechaLimiteLlamada: [null, Validators.required],
      fechaPronosticoProxLlamada: [null, Validators.required],
      fechaSugeridaClienteProxLlamada: [null, Validators.required],
      fechaCorregidaSistemaProxLlamada: [null, Validators.required]
    });
  }
  resetForm() {
    return {};
  }
  setHora(hora) {
    const h = timeTo24h(hora);
    let date = new Date();
    date.setHours(h.horas, Number(h.minutos), 0);
    return date;
  }
}
