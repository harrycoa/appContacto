import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { timeTo24h } from '@shared/functions';

@Component({
  selector: 'motivo-b10',
  templateUrl: './motivo-b10.component.html',
  styleUrls: ['./motivo-b10.component.scss']
})
export class MotivoB10Component implements OnInit {
  form: FormGroup;
  incumplimientos = INCUMPLIMIENTOS;

  constructor(private fb: FormBuilder) {}
  dayFilter = (d: Date) => d.getDay() != 0;

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      motivoDescripcion: [null],
      horaPosibleArribo: [null],
      estadoCita: ['S'],
      horaInicio: [null],
      horaEntrega: [null],
      fechaCitaReprogramada: [null],
      resumenContacto: [null],
      vozCliente: [null],
      razonNoCita: [null]
    });
  }
  changeIncumplimiento() {
    this.form.patchValue({
      fechaCitaReprogramada: null,
      resumenContacto: null,
      vozCliente: null,
      razonNoCita: null
    });
  }

  setHora(hora) {
    const h = timeTo24h(hora);
    let date = new Date();
    date.setHours(h.horas, Number(h.minutos), 0);
    return date;
  }
}

export const INCUMPLIMIENTOS = [
  'Reprogramar para el mismo día',
  'Reprogramar para otra fecha',
  'No volverá',
  'Confusión al reservar cita',
  'Otro'
];
