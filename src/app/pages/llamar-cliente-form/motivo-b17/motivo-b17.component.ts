import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'motivo-b17',
  templateUrl: './motivo-b17.component.html',
  styleUrls: ['./motivo-b17.component.scss']
})
export class MotivoB17Component implements OnInit {
  form:FormGroup;

  _idMotivo: number;

  @Input()
  set idMotivo(id) {
    this._idMotivo = id;
  }
  constructor(private fb:FormBuilder) { }

  ngOnInit() {
    this.initialize();
  }
  initialize() {
    this.form = this.fb.group(this.initForm());
  }
  reset() {
    this.form.reset();
  }
  initForm() {
    return {
      otro: [null, Validators.required],
      estadoContacto: ['P', Validators.required],
      vozCliente: [null, Validators.required]
    };
  }
  resetForm() {
    return {
      otro: null,
      estadoContacto: 'P',
      vozCliente: null
    };
  }
  changeRecojoResult(e){

  }
}
