import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { timeTo24h } from '@shared/functions';
import { TiposService } from 'app/pages/tipos.service';

@Component({
  selector: 'detalle-razon-no-cita',
  templateUrl: './detalle-razon-no-cita.component.html',
  styleUrls: ['./detalle-razon-no-cita.component.scss']
})
export class DetalleRazonNoCitaComponent implements OnInit {
  form: FormGroup;

  usosVehiculares = usoVehiculares;

  razonesRechazo = razonesRechazoCorasur;

  loading: boolean;

  private _placa;
  @Input()
  set placa(placa) {
    this._placa = placa;
  }
  get placa() {
    return this._placa;
  }

  private _razon;
  @Input()
  set razonNoAgendoCita(razon) {
    this._razon = razon;

    this.resetForm();

    if (razon == 'FALTA RECORRIDO') {
      this.loading = true;
      this.service.fechaLimite(this.placa).subscribe(
        (fecha: any) => {
          this.form.patchValue({ fechaLimiteLlamada: fecha.fechaLimite });
          this.loading = false;
        },
        _err => (this.loading = false)
      );
    }
  }
  get razonNoAgendoCita() {
    return this._razon;
  }

  constructor(private fb: FormBuilder, private service: TiposService) {}
  dayFilter = (d: Date) => d.getDay() !== 0;

  ngOnInit() {
    this.initForm();
  }

  reset() {
    this.resetForm();
  }

  initForm() {
    this.form = this.fb.group({
      vozCliente: [null, Validators.required],
      sabeKm: ['S', Validators.required],
      kmActual: [null, Validators.required],
      usoVehicular: [null, Validators.required],
      fechaLimiteLlamada: [null, Validators.required],
      fechaPronosticoProxLlamada: [null, Validators.required],
      fechaSugeridaClienteProxLlamada: [null],
      fechaCorregidaSistemaProxLlamada: [null],
      fechaPactadaProxLlamada: [null, Validators.required],
      resumenContacto: [null, Validators.required]
    });
  }
  resetForm() {
    if (this.form && this.form.reset)
      this.form.reset({
        vozcliente: null,
        sabeKm: 'S',
        kmActual: null,
        usoVehicular: null,
        fechaLimiteLlamada: null,
        fechaPronosticoProxLlamada: null,
        fechaSugeridaClienteProxLlamada: null,
        fechaCorregidaSistemaProxLlamada: null,
        fechaPactadaProxLlamada: null,
        resumenContacto: null
      });
  }

  changeKm(sabe) {
    if (sabe == 'S') this.form.patchValue({ usoVehicular: null });
    else this.form.patchValue({ kmActual: null });
  }

  calcularFechaPronostico() {
    const sabeKm = this.form.get('sabeKm').value;
    let km;
    if (sabeKm == 'S') km = this.form.get('kmActual').value;
    else km = this.form.get('usoVehicular').value;
    this.service
      .proximaLlamada(this.placa, sabeKm, km)
      .subscribe((f: any) =>
        this.form.patchValue({ fechaPronosticoProxLlamada: f.fechaLlamada })
      );
  }
  changeFechasugerida(fechaSugerida: Date) {
    const fechaPronosticoProxLlamada = new Date(
      this.form.get('fechaPronosticoProxLlamada').value
    );
    if (fechaSugerida.getTime() < fechaPronosticoProxLlamada.getTime())
      this.form.patchValue({ fechaCorregidaSistemaProxLlamada: fechaSugerida });
    else
      this.form.patchValue({
        fechaCorregidaSistemaProxLlamada: fechaPronosticoProxLlamada
      });
  }
}

const usoVehiculares = [
  'Empresa',
  'Gobierno',
  'Transporte',
  'Particular',
  'Taxista'
];

const razonesRechazoCorasur = [
  'Le parece muy caro',
  'Demora mucho',
  'Me trataron mal',
  'Lavaron mal mi unidad',
  'No realizaron el servicio solicitado',
  'Rasparon mi unidad',
  'Tomaron una pertenencia de mi unidad',
  'Otro'
];
