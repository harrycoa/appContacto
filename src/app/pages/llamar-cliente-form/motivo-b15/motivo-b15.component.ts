import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'motivo-b15',
  templateUrl: './motivo-b15.component.html',
  styleUrls: ['./motivo-b15.component.scss']
})
export class MotivoB15Component implements OnInit {
  form: FormGroup;

  registroAlertas = REGISTRO_ALERTAS;
  resultadoAlertas = RESULTADO_ALERTAS;
  atencionesDirectas = ATENCIONES_DIRECTAS;
  atencionesIndirectas = ATENCIONES_INDIRECTAS;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      motivoDescripcion: [null],
      vozCliente: [null],
      personaModificaCita: [null],
      razonNoCita: [null],
      resumenContacto: [null],
      fechaCitaCambiada: [null],
      aFreno: [null],
      bFreno: [null],
      cFreno: [null],
      dFreno: [null]
    });
  }
  changeResultadoAlerta() {
    this.form.patchValue({
      razonNoCita: null,
      resumenContacto: null,
      fechaCitaCambiada: null,
      aFreno: null,
      bFreno: null,
      cFreno: null,
      dFreno: null
    });
  }
  changeRegistroAlerta() {
    this.form.patchValue({
      resumenContacto: null,
      fechaCitaCambiada: null,
      aFreno: null,
      bFreno: null,
      cFreno: null,
      dFreno: null
    });
  }
}

const REGISTRO_ALERTAS = ['Ventas', 'Servicios', 'Repuestos'];

const RESULTADO_ALERTAS = [
  'Atención directa de voz cliente negativa',
  'Atención indirecta para retención de cliente'
];

const ATENCIONES_DIRECTAS = [
  'Aclaración inmediata documentada',
  'Aclaración inmediata no documentada',
  'Requiere retorno al taller para verificación de queja en taller',
  'Requiere retorno al taller para verificación de queja en prueba de ruta',
  'Otro'
];

const ATENCIONES_INDIRECTAS = ['Descuento', 'Servicio Gratuito', 'Otro'];
