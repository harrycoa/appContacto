import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'motivo-b12',
  templateUrl: './motivo-b12.component.html',
  styleUrls: ['./motivo-b12.component.scss']
})
export class MotivoB12Component implements OnInit {
  form: FormGroup;
  notificaciones = NOTIFICACIONES;
  motivosNoti;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
    this.motivosNoti = MOTIVOS;
  }

  initForm() {
    this.form = this.fb.group({
      motivoDescripcion: [null, Validators.required],
      fechaRecojoDocumento: [null, Validators.required],
      vozCliente: [null, Validators.required],
      razonNoCita: [null, Validators.required]
    });
  }
  resetForm() {
    this.form.reset({
      motivoDescripcion: null,
      fechaRecojoDocumento: null,
      vozCliente: null,
      razonNoCita: null
    });
  }
  changeNotificacion() {
    this.form.patchValue({
      fechaRecojoDocumento: null,
      vozCliente: null,
      razonNoCita: null
    });
  }
  changeFecha(fechaRecojoDocumento) {
    this.form.patchValue({ fechaRecojoDocumento });
  }
}

export const NOTIFICACIONES = [
  'Confirma fecha y hora de recojo, según cita',
  'Confirma fecha y hora de recojo, según OT',
  'Adelanto de fecha y hora de recojo, por termino de trabajo',
  'Mensaje de voz notificando el termino del servicio',
  'Mensaje de texto notificando el termino del servicio',
  'Mensaje de Whatsapp notificando el termino del servicio',
  'Desea postergar la fecha y hora entrega'
];

const MOTIVOS = ['Trabajo', 'Fuera', 'Salud', 'Emergencia'];
