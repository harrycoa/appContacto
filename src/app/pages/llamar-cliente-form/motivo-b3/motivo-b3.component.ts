import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  ServiciosCitaComponent,
  Servicio
} from '../servicios-cita/servicios-cita.component';
import { DetalleRazonNoCitaComponent } from '../detalle-razon-no-cita/detalle-razon-no-cita.component';

@Component({
  selector: 'motivo-b3',
  templateUrl: './motivo-b3.component.html',
  styleUrls: ['./motivo-b3.component.scss']
})
export class MotivoB3Component implements OnInit {
  titulo = '';
  form: FormGroup;

  private _idMotivo: number;
  @Input()
  set idMotivo(id) {
    this._idMotivo = id;
    this.setTitle(id);
  }
  get idMotivo() {
    return this._idMotivo;
  }

  private _placa;
  @Input()
  set placa(placa) {
    this._placa = placa;
  }
  get placa() {
    return this._placa;
  }

  @ViewChild('sC')
  sC: ServiciosCitaComponent;

  @ViewChild('dRnoC')
  dRnoC: DetalleRazonNoCitaComponent;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }
  setTitle(idMotivo) {
    switch (idMotivo) {
      case 17:
        this.titulo = 'B3. INVITACIÓN A 1K';
        break;
      case 18:
        this.titulo = 'B4. INVITACIÓN A 5K - 150k';
        break;
      case 20:
        this.titulo = 'B6. INVITACIÓN A CAMPAÑA';
        break;
    }
  }
  initForm() {
    this.form = this.fb.group({
      agendaCita: ['S', Validators.required],
      fechaCita: [null, Validators.required],
      razonNoCita: [null, Validators.required]
    });
  }
  reset() {
    if (this.form.get('agendaCita').value == 'N') this.dRnoC.form.reset();
    this.form.reset({
      agendaCita: 'S',
      fechaCita: null,
      razonNoCita: null
    });
  }

  changeAgendarCita(agendaCita) {
    this.form.patchValue({ agendaCita });
    this.form.patchValue({ fechaCita: null });
    this.form.patchValue({ razonNoCita: null });
    if (this.dRnoC && this.dRnoC.form) this.dRnoC.form.reset();
  }

  changeFecha(fechaCita) {
    this.form.patchValue({ fechaCita });
  }

  changeRazonNoCita(razonNoCita) {
    this.form.patchValue({ razonNoCita });
  }

  valueForm(): ValueMotivo {
    let form = new ValueMotivo();
    if (this.form.get('agendaCita').value == 'S') {
      form = { ...form, ...this.form.value };
      form.detalle = this.sC.servicios();
    } else form = { ...form, ...this.form.value, ...this.dRnoC.form.value };

    return form;
  }
}

export class ValueMotivo {
  agendaCita: string;
  fechaCita: string;

  detalle: Servicio[];
  razonNoCita: string;

  vozCliente: string;
  sabeKm: string;
  kmActual: number;
  usoVehicular: string;
  fechaLimiteLlamada: string;
  fechaPronosticoProxLlamada: string;
  fechaSugeridaClienteProxLlamada: string;
  fechaCorregidaSistemaProxLlamada: string;
  fechaPactadaProxLlamada: string;
  resumenContacto: string;
  constructor() {
    this.agendaCita = null;
    this.fechaCita = null;

    this.detalle = [];
    this.razonNoCita = null;

    this.vozCliente = null;
    this.sabeKm = null;
    this.kmActual = null;
    this.usoVehicular = null;
    this.fechaLimiteLlamada = null;
    this.fechaPronosticoProxLlamada = null;
    this.fechaSugeridaClienteProxLlamada = null;
    this.fechaCorregidaSistemaProxLlamada = null;
    this.fechaPactadaProxLlamada = null;
    this.resumenContacto = null;
  }
}
