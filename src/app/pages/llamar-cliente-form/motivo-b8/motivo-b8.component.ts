import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'motivo-b8',
  templateUrl: './motivo-b8.component.html',
  styleUrls: ['./motivo-b8.component.scss']
})
export class MotivoB8Component implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      estadoCita: ['E', Validators.required],
      fechaCita: [null],
      fechaCitaCambiada: [null],
      vozCliente: [null, Validators.required],
      razonModificaCita: [null, Validators.required],
      razonNoCita: [null, Validators.required]
    });
  }

  changeRadio() {
    this.form.patchValue({
      fechaCita: null,
      fechaCitaCambiada: null,
      vozCliente: null,
      razonModificaCita: null,
      razonNoCita: null
    });
  }
}
