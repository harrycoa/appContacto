import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { TiposService } from 'app/pages/tipos.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'gestion-voz-negativa-list',
  templateUrl: './gestion-voz-negativa-list.component.html',
  styleUrls: ['./gestion-voz-negativa-list.component.scss']
})
export class GestionVozNegativaListComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  loadingData: boolean;

  columnas: string[] = [
    'idContacto',
    'trabajadorCorasur',
    'placa',
    'cliente',
    'contactoCliente',
    'resultadoContacto',
    'motivo',
    'vozCliente',
    'fechaRegistro'
  ];

  @Output()
  select = new EventEmitter<any>();

  constructor(
    private service: TiposService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.paginator = this.paginator;
    this.loadingData = true;
    this.service.listarGestionVozNegativa().subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res as any);
        this.dataSource.paginator = this.paginator;
      },
      _err => (this.loadingData = false),
      () => (this.loadingData = false)
    );
  }

  selectRow(row) {
    localStorage.setItem('vozNegativa', JSON.stringify(row));
    this.router.navigate(['gestion'], { relativeTo: this.route });
  }
}
