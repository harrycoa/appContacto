import { Component, OnInit, ViewChild } from '@angular/core';
import { TiposService } from 'app/pages/tipos.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'gestion-voz-negativa',
  templateUrl: './gestion-voz-negativa.component.html',
  styleUrls: ['./gestion-voz-negativa.component.scss']
})
export class GestionVozNegativaComponent implements OnInit {
  @ViewChild('f')
  f: any;
  saving: boolean;
  constructor(
    private service: TiposService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {}

  save(datosForm) {
    this.saving = true;
    this.service.guardarGestionVozNegativa(datosForm).subscribe(
      _res => (this.saving = false),
      _err => (this.saving = false),
      () => {
        this.f.reset();
        this.back();
      }
    );
  }

  back() {
    localStorage.removeItem('vozNegativa');
    this.router.navigate(['/gestion-voz-negativa'], {
      relativeTo: this.route
    });
  }
}
