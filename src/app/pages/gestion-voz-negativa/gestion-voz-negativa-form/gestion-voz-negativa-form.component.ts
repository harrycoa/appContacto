import {
  Component,
  OnInit,
  ViewChild,
  EventEmitter,
  Output
} from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TiposService } from 'app/pages/tipos.service';
import { timeTo24h } from '@shared/functions';

@Component({
  selector: 'gestion-voz-negativa-form',
  templateUrl: './gestion-voz-negativa-form.component.html',
  styleUrls: ['./gestion-voz-negativa-form.component.scss']
})
export class GestionVozNegativaFormComponent implements OnInit {
  datosCliente;
  form: FormGroup;
  estados: any[];
  loadingEstados: boolean;

  isDirecta = true;
  listaDirecta = atencionDirecta;
  listaIndirecta = atencionIndirecta;

  horaRetornoCorasur;
  horaRetomarContacto;

  @ViewChild('f')
  f: any;

  @Output()
  enviar = new EventEmitter<any>();

  constructor(private fb: FormBuilder, private service: TiposService) {}

  dayFilter = (d: Date): boolean => d.getDay() !== 0;

  ngOnInit() {
    this.initialize();
  }
  initialize() {
    this.datosCliente = {
      placa: null,
      cliente: null,
      contactoCliente: null
    };
    this.estados = [];

    this.horaRetomarContacto = '12:00 PM';
    this.horaRetornoCorasur = '12:00 PM';
    this.form = this.fb.group(this.initForm());

    if (localStorage.getItem('vozNegativa') != null) {
      const data = JSON.parse(localStorage.getItem('vozNegativa'));
      this.datosCliente = data;
      this.form.patchValue({
        placa: data.placa,
        cliente: data.cliente,
        contactoCliente: data.contactoCliente,
        idContacto: data.idContacto
      });
    }
    this.changeEstadoLlamada();
  }

  initForm() {
    return {
      placa: [null, Validators.required],
      cliente: [null, Validators.required],
      contactoCliente: [null, Validators.required],
      idContacto: [null, Validators.required],
      activo: [null, Validators.required],
      idEstado: [null, Validators.required],
      resultadoContacto: [null],
      fechaRetomarContacto: [null],
      atendidoGestion: ['D'],
      indicacionSugerida: [null],
      detalleGestion: [null],
      fechaRetornoCorasur: [null],
      tipoDescuento: [null],
      caducidad: [null],
      tipoServicio: [null],
      servicio: [null]
    };
  }
  resetForm() {
    return {
      placa: null,
      cliente: null,
      contactoCliente: null,
      activo: null,
      idEstado: null,
      resultadoContacto: null,
      fechaRetomarContacto: null,
      atendidoGestion: 'D',
      indicacionSugerida: null,
      detalleGestion: null,
      fechaRetornoCorasur: null,
      tipoDescuento: null,
      caducidad: null,
      tipoServicio: null,
      servicio: null,
      idContacto: null
    };
  }

  reset() {
    this.f.resetForm();
    this.horaRetomarContacto = '12:00 PM';
    this.horaRetornoCorasur = '12:00 PM';
    this.form.reset(this.resetForm());
    this.datosCliente = {
      placa: null,
      cliente: null,
      contactoCliente: null
    };
  }

  getEstados(clase) {
    this.loadingEstados = true;
    this.service.listarEstadosPorClase(clase).subscribe(
      (res: any[]) => {
        this.loadingEstados = false;
        this.estados = res;
      },
      _err => (this.loadingEstados = false)
    );
  }

  changeEstadoLlamada() {
    this.form.get('activo').valueChanges.subscribe(activo => {
      const clase = activo == 'S' ? 'A' : 'B';
      this.getEstados(clase);
    });
  }

  formatToDate(timeStr: string) {
    const colon = timeStr.indexOf(':');
    const hours = timeStr.substr(0, colon),
      minutes = timeStr.substr(colon + 1, 2),
      meridian = timeStr.substr(colon + 4, 2).toUpperCase();
    let hoursInt = parseInt(hours, 10);
    const offset = meridian == 'PM' ? 12 : 0;

    if (hoursInt === 12) hoursInt = offset;
    else hoursInt += offset;

    const fechaRetomar: Date = this.form.get('fechaRetomar').value as Date;
    fechaRetomar.setHours(hoursInt, Number(minutes), 0);
    this.form.patchValue({ fechaRetomar });
  }

  changeAtencion(atendidoGestion) {
    this.form.patchValue({
      atendidoGestion,
      indicacionSugerida: null,
      detalleGestion: null,
      fechaRetornoCorasur: null,
      tipoDescuento: null,
      caducidad: null,
      tipoServicio: null,
      servicio: null
    });
  }
  onSubmit() {
    this.enviar.emit(this.form.value);
  }

  changeFechaRetomar(fecha) {
    this.setFechaHoraRetomar();
  }
  changeHoraRetomar(hora) {
    this.horaRetomarContacto = hora;
    if (this.form.get('fechaRetomarContacto').value != null)
      this.setFechaHoraRetomar();
  }
  setFechaHoraRetomar() {
    const h = timeTo24h(this.horaRetomarContacto);
    const fechaRetomarContacto: Date = this.form.get('fechaRetomarContacto')
      .value as Date;
    fechaRetomarContacto.setHours(h.horas, Number(h.minutos), 0);
    this.form.patchValue({ fechaRetomarContacto });
  }

  changeFechaRetorno(fecha) {
    this.setFechaHoraRetorno();
  }
  changeHoraRetorno(hora) {
    this.horaRetornoCorasur = hora;
    if (this.form.get('fechaRetornoCorasur').value != null)
      this.setFechaHoraRetorno();
  }
  setFechaHoraRetorno() {
    const h = timeTo24h(this.horaRetornoCorasur);
    const fechaRetornoCorasur: Date = this.form.get('fechaRetornoCorasur')
      .value as Date;
    fechaRetornoCorasur.setHours(h.horas, Number(h.minutos), 0);
    this.form.patchValue({ fechaRetornoCorasur });
  }

  subirArchivo() {}
}

const atencionDirecta = [
  'Aclaración inmediata documentada',
  'Aclaración inmediata no documentada',
  'Requiere retorno a taller, queja en taller',
  'Requiere retorno a taller, prueba de ruta',
  'Otros'
];
const atencionIndirecta = ['Descuento', 'Servicio Gratuito', 'Otros'];
