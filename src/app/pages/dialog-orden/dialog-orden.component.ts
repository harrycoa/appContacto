import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatPaginator
} from '@angular/material';
import { OrdenReparacion } from '@models/ordenReparacion';

@Component({
  selector: 'app-dialog-orden',
  templateUrl: './dialog-orden.component.html',
  styleUrls: ['./dialog-orden.component.scss']
})
export class DialogOrdenComponent implements OnInit {
  displayedColumns: string[] = [
    'Placa',
    'Orden',
    'Cliente',
    'Km',
    'Servicio',
    'FechaInicio',
    'FechaFin'
  ];
  dataSource: MatTableDataSource<OrdenReparacion>;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  constructor(
    public dialogRef: MatDialogRef<DialogOrdenComponent>,
    @Inject(MAT_DIALOG_DATA) public ordenesReparacion: OrdenReparacion[]
  ) {
    dialogRef.backdropClick().subscribe(() => dialogRef.close());
    dialogRef.keydownEvents().subscribe(e => {
      if (e.keyCode == 27) dialogRef.close();
    });
  }
  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.ordenesReparacion);
    this.dataSource.paginator = this.paginator;
  }

  TrackByFunction(index) {
    return index;
  }
}
