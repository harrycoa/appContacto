import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { EncuestaComponent } from './encuesta/encuesta.component';

import { Cita } from '@models/cita';
import {
  ListPostergaciones,
  ListRecordatorios,
  ListRazonesCambioCita,
  ListMotivosCancelaCita,
  ListIncumplimientos
} from '@shared/lists/lists';
import { DatosBasicosComponent } from './datos-basicos/datos-basicos.component';
import { timeTo24h } from '@shared/functions';
import { MotivoEstadoComponent } from './motivo-estado/motivo-estado.component';
import { MotivoB1Component } from './motivo-b1/motivo-b1.component';

@Component({
  selector: 'llamar-cliente-form',
  templateUrl: './llamar-cliente-form.component.html',
  styleUrls: ['./llamar-cliente-form.component.scss']
})
export class LlamarClienteFormComponent implements OnInit {
  recojos;
  invitaciones;
  notificaciones;
  aprobaciones;
  servicios;
  estados: any[];
  respuestas: any[];

  @Output()
  enviar = new EventEmitter<any>();

  form: FormGroup;

  _respuestas;

  @ViewChild('f')
  f: any;

  // SERVICIOS - CITA
  citas: Cita[];
  mantenimiento: string;
  diagnostico: string;
  reparacion: string;
  idRespuesta: string;
  byp: string;
  garantia: string;
  ssc: string;
  insAccesorio: string;
  otro: string;

  horaCita;
  horaRecordatorio;
  horaFinal;
  horaSgteLlamada;
  // Notificacion
  motivosNoti;
  // Registros de alertas TDP
  registroAlertas;
  resultadoAlertas;
  atencionesDirectas;
  atencionesIndirectas;

  postergaciones = ListPostergaciones;
  recordatorios = ListRecordatorios;
  razonesCambioCita = ListRazonesCambioCita;
  motivosCancelaCita = ListMotivosCancelaCita;
  incumplimientos = ListIncumplimientos;
  motivosIncumplimiento: Array<any> = [];

  @ViewChild('datosBasicos')
  datosBasicos: DatosBasicosComponent;

  @ViewChild('motivoEstado')
  motivoEstado: MotivoEstadoComponent;
  idMotivo;
  activo;

  @ViewChild('b1')
  b1: MotivoB1Component;

  constructor(private fb: FormBuilder, public dialog: MatDialog) {}

  ngOnInit() {
    this.initialize();

    this.horaCita = '12:00 PM';
    this.horaRecordatorio = '12:00 PM';
    this.horaFinal = '12:00 PM';
    this.horaSgteLlamada = '12:00 PM';
    this.form = this.fb.group(this.initForm());

    this.citas = [];
    this.mantenimiento = '0';
    this.motivosNoti = [
      { id: 1, motivo: 'TRABAJO' },
      { id: 2, motivo: 'FUERA' },
      { id: 3, motivo: 'SALUD' },
      { id: 4, motivo: 'EMERGENCIA' }
    ];
    this.registroAlertas = [
      { idReA: 1, registro: 'VENTAS' },
      { idReA: 2, registro: 'SERVICIOS' },
      { idReA: 3, registro: 'REPUESTOS' }
    ];
    this.resultadoAlertas = [
      { idRA: 1, resultado: 'ATENCION DIRECTA DE VOZ DE CLIENTE NEGATIVA' },
      { idRA: 2, resultado: 'ATENCION INDIRECTA PARA RETENCION DE CLIENTE' }
    ];
    this.atencionesDirectas = [
      { idAD: 1, atencion: 'ACLARACION INMEDIATA DOCUMENTADA' },
      { idAD: 2, atencion: 'ACLARACION INMEDIATA NO DOCUMENTADA' },
      {
        idAD: 3,
        atencion:
          'REQUIERE RETORNO AL TALLER PARA VERIFICACION DE QUEJA EN TALLER'
      },
      {
        idAD: 4,
        atencion:
          'REQUIERE RETORNO AL TALLER PARA VERIFICACION DE QUEJA EN PRUEBA DE RUTA'
      },
      { idAD: 5, atencion: 'OTRO' }
    ];
    this.atencionesIndirectas = [
      { idID: 1, atencion: 'DESCUENTO' },
      { idID: 2, atencion: 'SERVICIO GRATUITO' },
      { idID: 3, atencion: 'OTRO' }
    ];
  }
  initialize() {}

  initForm() {
    return {
      vozNegativaRecojo: [''],
      vozNegativaRecordatorio: [''],
      detalleInvitacion: '',
      idMotivo: [null, Validators.required],
      idRecojo: [null],
      idInvitacion: [null],
      idNotificacion: [null],
      idAprobacion: [null],
      idServicio: [null],
      idPostergacion: [null],
      idRecordatorio: [null],
      idIncumplimiento: [null],
      idMI: [null],
      idRCC: [null],
      idMCC: [null],
      idRA: [null],
      idReA: [null],
      idID: [null],
      idAD: [null],
      id: [null],
      k: [{ value: null, disabled: true }, Validators.required],
      activo: ['', Validators.required],
      idEstado: [null, Validators.required],
      km: [0, Validators.required],
      resultado: [''], // OBSERVACIONES
      fechaRetomar: null,
      nuevoDuenio: null,
      telefonoNuevoDuenio: null,
      agendaCita: 'S',
      arriboCita: 'E',
      invitacionCita: 'S',
      resultAgradecimiento: 'P',
      recojoTarjeta: 'P',
      fechaCita: null,
      fechaRecojo: null,
      fechaRecordatorio: null,
      descripcionNoCita: null,
      razonNoCita: null,
      sabeKm: null,
      kmFaltaRecorrido: null,
      sabePromedio: null,
      promedioRecorrido: null,
      usoVehicular: null,
      fechaPropuesta: null,
      fechaSugeridaCliente: null,
      fechaCorregida: null,
      fechaFinal: null,
      fechaSiguienteLlamada: null,
      concesionario: null,
      causaRechazoCita: null,
      causaRechazoCorasur: null,
      observacionesNoCita: null
    };
  }
  resetForm() {
    this.citas = [];
    this.mantenimiento = null;
    this.diagnostico = null;
    this.reparacion = null;
    this.idRespuesta = null;
    this.byp = null;
    this.garantia = null;
    this.ssc = null;
    this.insAccesorio = null;
    this.otro = null;

    this.horaCita = '12:00 PM';
    this.horaRecordatorio = '12:00 PM';
    this.horaFinal = '12:00 PM';
    this.horaSgteLlamada = '12:00 PM';

    return {
      vozNegativaRecojo: [''],
      vozNegativa: [{ value: '', disabled: true }],
      vozPositiva: '',
      detalleInvitacion: '',
      idMotivo: null,
      idRecojo: null,
      idInvitacion: [null],
      idNotificacion: [null],
      idAprobacion: [null],
      idIncumplimiento: [null],
      idServicio: [null],
      idPostergacion: [null],
      idRecordatorio: [null],
      idMI: [null],
      idRCC: [null],
      idMCC: [null],
      idRA: [null],
      idReA: [null],
      idID: [null],
      idAD: [null],
      id: [null],
      k: 0,
      activo: '',
      idEstado: null,
      km: 0,
      resultado: '', // OBSERVACIONES
      fechaRetomar: null,
      nuevoDuenio: null,
      telefonoNuevoDuenio: null,
      agendaCita: 'S',
      arriboCita: 'E',
      invitacionCita: 'S',
      resultAgradecimiento: 'P',
      recojoTarjeta: 'P',
      fechaCita: null,
      fechaRecojo: null,
      fechaRecordatorio: null,
      descripcionNoCita: null,
      razonNoCita: null,
      sabeKm: null,
      kmFaltaRecorrido: null,
      sabePromedio: null,
      promedioRecorrido: null,
      usoVehicular: null,
      fechaPropuesta: null,
      fechaSugeridaCliente: null,
      fechaCorregida: null,
      fechaFinal: null,
      fechaSiguienteLlamada: null,
      concesionario: null,
      causaRechazoCita: null,
      causaRechazoCorasur: null,
      observacionesNoCita: null
    };
  }

  changeRecojoResult(event) {
    if (event.value === 'P')
      this.form.controls['vozNegativaRecojo'].setValue('');
  }
  changeResultAgradecimiento(event) {
    if (event.value === 'N') {
      this.form.controls['vozPositiva'].disable();
      this.form.controls['vozNegativa'].enable();
    } else {
      this.form.controls['vozPositiva'].enable();
      this.form.controls['vozNegativa'].disable();
    }
  }
  changeRecojo() {}
  changeInvitacion() {}
  changeNotificacion() {}
  changeServicios() {
    const dialogRef = this.dialog.open(EncuestaComponent, {
      maxWidth: '640px',
      minWidth: '320px',
      disableClose: true,
      data: {
        info: this.form.value.idServicio
      }
    });

    dialogRef
      .afterClosed()
      .pipe(reload => reload)
      .subscribe(reload => {
        if (reload) {
        }
      });
  }

  onSubmit() {
    console.log(this.datosBasicos.form.value);
    console.log(this.motivoEstado.form.value);
    console.log(this.b1.form.value);

    if (this.form.valid) {
      // if (this.form.get('fechaRetomar').value != null)
      //   this.setFechaHoraRetomar();
      if (this.form.get('fechaFinal').value != null) this.setFechaHoraFinal();
      if (this.form.get('fechaSiguienteLlamada').value != null)
        this.setFechaHoraSgteLlamada();

      // if (this.form.get('agendaCita').value == 'S')
      //   if (this.form.get('fechaCita').value != null) this.setCita();
      //   else this.citas = [];
      // else this.citas = [];

      //  this.enviar.emit(this.form.value);
      const form = {
        contacto: {
          placa: this.form.get('placa').value,
          trabajadorCorasur: this.form.get('trabajadorCorasur').value,
          tipoContactot: this.form.get('tipoContactot').value,
          contactot: this.form.get('contactot').value,
          cliente: this.form.get('cliente').value,
          tipoContactoc: this.form.get('tipoContactoc').value,
          contactoc: this.form.get('contactoc').value,
          idMotivo: this.form.get('idMotivo').value,
          k: this.form.get('k').value,
          activo: this.form.get('activo').value,
          idEstado: this.form.get('idEstado').value,
          resultado: this.form.get('resultado').value,
          km: this.form.get('km').value,
          fechaRetomar: this.form.get('fechaRetomar').value,
          nuevoDuenio: this.form.get('nuevoDuenio').value,
          telefonoNuevoDuenio: this.form.get('telefonoNuevoDuenio').value,
          agendaCita: this.form.get('agendaCita').value
        },
        cita: this.citas,
        razon: {
          descripcionNoCita: this.form.get('descripcionNoCita').value,
          razonNoCita: this.form.get('razonNoCita').value,
          sabeKm: this.form.get('sabeKm').value,
          kmFaltaRecorrido: this.form.get('kmFaltaRecorrido').value,
          sabePromedio: this.form.get('sabePromedio').value,
          promedioRecorrido: this.form.get('promedioRecorrido').value,
          usoVehicular: this.form.get('usoVehicular').value,
          fechaPropuesta: this.form.get('fechaPropuesta').value,
          fechaSugeridaCliente: this.form.get('fechaSugeridaCliente').value,
          fechaCorregida: this.form.get('fechaCorregida').value,
          fechaFinal: this.form.get('fechaFinal').value,
          fechaSiguienteLlamada: this.form.get('fechaSiguienteLlamada').value,
          concesionario: this.form.get('concesionario').value,
          causaRechazoCita: this.form.get('causaRechazoCita').value,
          causaRechazoCorasur: this.form.get('causaRechazoCorasur').value,
          observacionesNoCita: this.form.get('observacionesNoCita').value
        }
      };

      // this.enviar.emit(this.form.value);
      this.enviar.emit(form);
      this.form.patchValue({ descripcionServicio: false });
    }
  }
  reset() {
    this.f.resetForm();
    // this.horaRetomar = '12:00 PM';
    this.horaCita = '12:00 PM';
    this.horaRecordatorio = '12:00 PM';
    this.horaFinal = '12:00 PM';
    this.horaSgteLlamada = '12:00 PM';
    this.form.reset(this.resetForm());
  }

  setFechaHoraFinal() {
    const h = timeTo24h(this.horaFinal);
    const fechaFinal: Date = this.form.get('fechaFinal').value as Date;
    fechaFinal.setHours(h.horas, Number(h.minutos), 0);
    this.form.patchValue({ fechaFinal });
  }

  setFechaHoraSgteLlamada() {
    const h = timeTo24h(this.horaSgteLlamada);
    const fechaSgteLlamda: Date = this.form.get('fechaSgteLlamda')
      .value as Date;
    fechaSgteLlamda.setHours(h.horas, Number(h.minutos), 0);
    this.form.patchValue({ fechaSgteLlamda });
  }

  changeMotivoActivo(e) {
    console.log(e);

    this.idMotivo = e.idMotivo;
    this.activo = e.activo;
  }
}
